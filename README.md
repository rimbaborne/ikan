## Aplikasi Ikan - Palembang. Project Tukang Kode

[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-boilerplate)
[![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-boilerplate) 
<br/>
[![StyleCI](https://styleci.io/repos/30171828/shield?style=plastic)](https://styleci.io/repos/30171828/shield?style=plastic)
[![CircleCI](https://circleci.com/gh/rappasoft/laravel-boilerplate/tree/master.svg?style=svg)](https://circleci.com/gh/rappasoft/laravel-boilerplate/tree/master)
<br/>
![GitHub contributors](https://img.shields.io/github/contributors/rappasoft/laravel-boilerplate.svg)
![GitHub stars](https://img.shields.io/github/stars/rappasoft/laravel-boilerplate.svg?style=social)


### Official Documentation for Template

[Click here for the official documentation](http://laravel-boilerplate.com)

### Creator

Tukang Kode. Gagang & Eko

### Deskripsi

Project tentang buatan transaksi dagang Ikan.

### License

MIT: [http://anthony.mit-license.org](http://anthony.mit-license.org)
