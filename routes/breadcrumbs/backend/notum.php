<?php

Breadcrumbs::for('admin.nota.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_nota.labels.management'), route('admin.nota.index'));
});

Breadcrumbs::for('admin.nota.create', function ($trail) {
    $trail->parent('admin.nota.index');
    $trail->push(__('backend_nota.labels.create'), route('admin.nota.create'));
});

Breadcrumbs::for('admin.nota.show', function ($trail, $id) {
    $trail->parent('admin.nota.index');
    $trail->push(__('backend_nota.labels.view'), route('admin.nota.show', $id));
});

Breadcrumbs::for('admin.nota.edit', function ($trail, $id) {
    $trail->parent('admin.nota.index');
    $trail->push(__('backend.nota.labels.edit'), route('admin.nota.edit', $id));
});

Breadcrumbs::for('admin.nota.deleted', function ($trail) {
    $trail->parent('admin.nota.index');
    $trail->push(__('backend_nota.labels.deleted'), route('admin.nota.deleted'));
});
