<?php

Breadcrumbs::for('admin.stok_barangs.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_stok_barangs.labels.management'), route('admin.stok_barangs.index'));
});

Breadcrumbs::for('admin.stok_barangs.create', function ($trail) {
    $trail->parent('admin.stok_barangs.index');
    $trail->push(__('backend_stok_barangs.labels.create'), route('admin.stok_barangs.create'));
});

Breadcrumbs::for('admin.stok_barangs.show', function ($trail, $id) {
    $trail->parent('admin.stok_barangs.index');
    $trail->push(__('backend_stok_barangs.labels.view'), route('admin.stok_barangs.show', $id));
});

Breadcrumbs::for('admin.stok_barangs.edit', function ($trail, $id) {
    $trail->parent('admin.stok_barangs.index');
    $trail->push(__('backend.stok_barangs.labels.edit'), route('admin.stok_barangs.edit', $id));
});

Breadcrumbs::for('admin.stok_barangs.deleted', function ($trail) {
    $trail->parent('admin.stok_barangs.index');
    $trail->push(__('backend_stok_barangs.labels.deleted'), route('admin.stok_barangs.deleted'));
});
