<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';

require __DIR__.'/barang.php';
require __DIR__.'/jenis_barang.php';
require __DIR__.'/stok_barang.php';
require __DIR__.'/transaksi.php';
require __DIR__.'/notum.php';
require __DIR__.'/piutang.php';
require __DIR__.'/pelanggan.php';
require __DIR__.'/pengguna.php';
require __DIR__.'/karyawan.php';
require __DIR__.'/cabang.php';