<?php

Breadcrumbs::for('admin.karyawans.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_karyawans.labels.management'), route('admin.karyawans.index'));
});

Breadcrumbs::for('admin.karyawans.create', function ($trail) {
    $trail->parent('admin.karyawans.index');
    $trail->push(__('backend_karyawans.labels.create'), route('admin.karyawans.create'));
});

Breadcrumbs::for('admin.karyawans.show', function ($trail, $id) {
    $trail->parent('admin.karyawans.index');
    $trail->push(__('backend_karyawans.labels.view'), route('admin.karyawans.show', $id));
});

Breadcrumbs::for('admin.karyawans.edit', function ($trail, $id) {
    $trail->parent('admin.karyawans.index');
    $trail->push(__('backend.karyawans.labels.edit'), route('admin.karyawans.edit', $id));
});

Breadcrumbs::for('admin.karyawans.deleted', function ($trail) {
    $trail->parent('admin.karyawans.index');
    $trail->push(__('backend_karyawans.labels.deleted'), route('admin.karyawans.deleted'));
});
