<?php

Breadcrumbs::for('admin.jenis_barangs.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_jenis_barangs.labels.management'), route('admin.jenis_barangs.index'));
});

Breadcrumbs::for('admin.jenis_barangs.create', function ($trail) {
    $trail->parent('admin.jenis_barangs.index');
    $trail->push(__('backend_jenis_barangs.labels.create'), route('admin.jenis_barangs.create'));
});

Breadcrumbs::for('admin.jenis_barangs.show', function ($trail, $id) {
    $trail->parent('admin.jenis_barangs.index');
    $trail->push(__('backend_jenis_barangs.labels.view'), route('admin.jenis_barangs.show', $id));
});

Breadcrumbs::for('admin.jenis_barangs.edit', function ($trail, $id) {
    $trail->parent('admin.jenis_barangs.index');
    $trail->push(__('backend.jenis_barangs.labels.edit'), route('admin.jenis_barangs.edit', $id));
});

Breadcrumbs::for('admin.jenis_barangs.deleted', function ($trail) {
    $trail->parent('admin.jenis_barangs.index');
    $trail->push(__('backend_jenis_barangs.labels.deleted'), route('admin.jenis_barangs.deleted'));
});
