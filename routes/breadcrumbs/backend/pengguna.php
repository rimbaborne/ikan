<?php

Breadcrumbs::for('admin.penggunas.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_penggunas.labels.management'), route('admin.penggunas.index'));
});

Breadcrumbs::for('admin.penggunas.create', function ($trail) {
    $trail->parent('admin.penggunas.index');
    $trail->push(__('backend_penggunas.labels.create'), route('admin.penggunas.create'));
});

Breadcrumbs::for('admin.penggunas.show', function ($trail, $id) {
    $trail->parent('admin.penggunas.index');
    $trail->push(__('backend_penggunas.labels.view'), route('admin.penggunas.show', $id));
});

Breadcrumbs::for('admin.penggunas.edit', function ($trail, $id) {
    $trail->parent('admin.penggunas.index');
    $trail->push(__('backend.penggunas.labels.edit'), route('admin.penggunas.edit', $id));
});

Breadcrumbs::for('admin.penggunas.deleted', function ($trail) {
    $trail->parent('admin.penggunas.index');
    $trail->push(__('backend_penggunas.labels.deleted'), route('admin.penggunas.deleted'));
});
