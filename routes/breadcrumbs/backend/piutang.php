<?php

Breadcrumbs::for('admin.piutangs.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_piutangs.labels.management'), route('admin.piutangs.index'));
});

Breadcrumbs::for('admin.piutangs.create', function ($trail) {
    $trail->parent('admin.piutangs.index');
    $trail->push(__('backend_piutangs.labels.create'), route('admin.piutangs.create'));
});

Breadcrumbs::for('admin.piutangs.show', function ($trail, $id) {
    $trail->parent('admin.piutangs.index');
    $trail->push(__('backend_piutangs.labels.view'), route('admin.piutangs.show', $id));
});

Breadcrumbs::for('admin.piutangs.edit', function ($trail, $id) {
    $trail->parent('admin.piutangs.index');
    $trail->push(__('backend.piutangs.labels.edit'), route('admin.piutangs.edit', $id));
});

Breadcrumbs::for('admin.piutangs.deleted', function ($trail) {
    $trail->parent('admin.piutangs.index');
    $trail->push(__('backend_piutangs.labels.deleted'), route('admin.piutangs.deleted'));
});
