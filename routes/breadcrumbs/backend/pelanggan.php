<?php

Breadcrumbs::for('admin.pelanggans.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_pelanggans.labels.management'), route('admin.pelanggans.index'));
});

Breadcrumbs::for('admin.pelanggans.create', function ($trail) {
    $trail->parent('admin.pelanggans.index');
    $trail->push(__('backend_pelanggans.labels.create'), route('admin.pelanggans.create'));
});

Breadcrumbs::for('admin.pelanggans.show', function ($trail, $id) {
    $trail->parent('admin.pelanggans.index');
    $trail->push(__('backend_pelanggans.labels.view'), route('admin.pelanggans.show', $id));
});

Breadcrumbs::for('admin.pelanggans.edit', function ($trail, $id) {
    $trail->parent('admin.pelanggans.index');
    $trail->push(__('backend.pelanggans.labels.edit'), route('admin.pelanggans.edit', $id));
});

Breadcrumbs::for('admin.pelanggans.deleted', function ($trail) {
    $trail->parent('admin.pelanggans.index');
    $trail->push(__('backend_pelanggans.labels.deleted'), route('admin.pelanggans.deleted'));
});
