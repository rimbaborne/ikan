<?php

Breadcrumbs::for('admin.transaksis.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_transaksis.labels.management'), route('admin.transaksis.index'));
});

Breadcrumbs::for('admin.transaksis.create', function ($trail) {
    $trail->parent('admin.transaksis.index');
    $trail->push(__('backend_transaksis.labels.create'), route('admin.transaksis.create'));
});

Breadcrumbs::for('admin.transaksis.show', function ($trail, $id) {
    $trail->parent('admin.transaksis.index');
    $trail->push(__('backend_transaksis.labels.view'), route('admin.transaksis.show', $id));
});

Breadcrumbs::for('admin.transaksis.edit', function ($trail, $id) {
    $trail->parent('admin.transaksis.index');
    $trail->push(__('backend.transaksis.labels.edit'), route('admin.transaksis.edit', $id));
});

Breadcrumbs::for('admin.transaksis.deleted', function ($trail) {
    $trail->parent('admin.transaksis.index');
    $trail->push(__('backend_transaksis.labels.deleted'), route('admin.transaksis.deleted'));
});
