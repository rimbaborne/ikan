<?php

Breadcrumbs::for('admin.cabangs.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_cabangs.labels.management'), route('admin.cabangs.index'));
});

Breadcrumbs::for('admin.cabangs.create', function ($trail) {
    $trail->parent('admin.cabangs.index');
    $trail->push(__('backend_cabangs.labels.create'), route('admin.cabangs.create'));
});

Breadcrumbs::for('admin.cabangs.show', function ($trail, $id) {
    $trail->parent('admin.cabangs.index');
    $trail->push(__('backend_cabangs.labels.view'), route('admin.cabangs.show', $id));
});

Breadcrumbs::for('admin.cabangs.edit', function ($trail, $id) {
    $trail->parent('admin.cabangs.index');
    $trail->push(__('backend.cabangs.labels.edit'), route('admin.cabangs.edit', $id));
});

Breadcrumbs::for('admin.cabangs.deleted', function ($trail) {
    $trail->parent('admin.cabangs.index');
    $trail->push(__('backend_cabangs.labels.deleted'), route('admin.cabangs.deleted'));
});
