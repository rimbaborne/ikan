<?php

Breadcrumbs::for('admin.barangs.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('backend_barangs.labels.management'), route('admin.barangs.index'));
});

Breadcrumbs::for('admin.barangs.create', function ($trail) {
    $trail->parent('admin.barangs.index');
    $trail->push(__('backend_barangs.labels.create'), route('admin.barangs.create'));
});

Breadcrumbs::for('admin.barangs.show', function ($trail, $id) {
    $trail->parent('admin.barangs.index');
    $trail->push(__('backend_barangs.labels.view'), route('admin.barangs.show', $id));
});

Breadcrumbs::for('admin.barangs.edit', function ($trail, $id) {
    $trail->parent('admin.barangs.index');
    $trail->push(__('backend.barangs.labels.edit'), route('admin.barangs.edit', $id));
});

Breadcrumbs::for('admin.barangs.deleted', function ($trail) {
    $trail->parent('admin.barangs.index');
    $trail->push(__('backend_barangs.labels.deleted'), route('admin.barangs.deleted'));
});
