<?php

use App\Http\Controllers\Backend\KaryawanController;

use App\Models\Karyawan;

Route::bind('karyawan', function ($value) {
	$karyawan = new Karyawan;

	return Karyawan::withTrashed()->where($karyawan->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'karyawan'], function () {
	Route::get(	'', 		[KaryawanController::class, 'index']		)->name('karyawans.index');
    Route::get(	'create', 	[KaryawanController::class, 'create']	)->name('karyawans.create');
	Route::post('store', 	[KaryawanController::class, 'store']		)->name('karyawans.store');
    Route::get(	'deleted', 	[KaryawanController::class, 'deleted']	)->name('karyawans.deleted');
});

Route::group(['prefix' => 'karyawan/{karyawan}'], function () {
	// Karyawan
	Route::get('/', [KaryawanController::class, 'show'])->name('karyawans.show');
	Route::get('edit', [KaryawanController::class, 'edit'])->name('karyawans.edit');
	Route::patch('update', [KaryawanController::class, 'update'])->name('karyawans.update');
	Route::delete('destroy', [KaryawanController::class, 'destroy'])->name('karyawans.destroy');
	// Deleted
	Route::get('restore', [KaryawanController::class, 'restore'])->name('karyawans.restore');
	Route::get('delete', [KaryawanController::class, 'delete'])->name('karyawans.delete-permanently');
});