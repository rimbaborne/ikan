<?php

use App\Http\Controllers\Backend\NotumController;

use App\Models\Notum;

Route::bind('notum', function ($value) {
	$notum = new Notum;

	return Notum::withTrashed()->where($notum->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'nota'], function () {
	Route::get(	'', 		[NotumController::class, 'index']		)->name('nota.index');
    Route::get(	'create', 	[NotumController::class, 'create']	)->name('nota.create');
	Route::post('store', 	[NotumController::class, 'store']		)->name('nota.store');
    Route::get(	'deleted', 	[NotumController::class, 'deleted']	)->name('nota.deleted');
});

Route::group(['prefix' => 'nota/{notum}'], function () {
	// Notum
	Route::get('/', [NotumController::class, 'show'])->name('nota.show');
	Route::get('edit', [NotumController::class, 'edit'])->name('nota.edit');
	Route::patch('update', [NotumController::class, 'update'])->name('nota.update');
	Route::delete('destroy', [NotumController::class, 'destroy'])->name('nota.destroy');
	// Deleted
	Route::get('restore', [NotumController::class, 'restore'])->name('nota.restore');
	Route::get('delete', [NotumController::class, 'delete'])->name('nota.delete-permanently');
});