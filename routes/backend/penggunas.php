<?php

use App\Http\Controllers\Backend\PenggunaController;

use App\Models\Pengguna;

Route::bind('pengguna', function ($value) {
	$pengguna = new Pengguna;

	return Pengguna::withTrashed()->where($pengguna->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'penggunas'], function () {
	Route::get(	'', 		[PenggunaController::class, 'index']		)->name('penggunas.index');
    Route::get(	'create', 	[PenggunaController::class, 'create']	)->name('penggunas.create');
	Route::post('store', 	[PenggunaController::class, 'store']		)->name('penggunas.store');
    Route::get(	'deleted', 	[PenggunaController::class, 'deleted']	)->name('penggunas.deleted');
});

Route::group(['prefix' => 'penggunas/{pengguna}'], function () {
	// Pengguna
	Route::get('/', [PenggunaController::class, 'show'])->name('penggunas.show');
	Route::get('edit', [PenggunaController::class, 'edit'])->name('penggunas.edit');
	Route::patch('update', [PenggunaController::class, 'update'])->name('penggunas.update');
	Route::delete('destroy', [PenggunaController::class, 'destroy'])->name('penggunas.destroy');
	// Deleted
	Route::get('restore', [PenggunaController::class, 'restore'])->name('penggunas.restore');
	Route::get('delete', [PenggunaController::class, 'delete'])->name('penggunas.delete-permanently');
});