<?php

use App\Http\Controllers\Backend\CabangController;

use App\Models\Cabang;

Route::bind('cabang', function ($value) {
	$cabang = new Cabang;

	return Cabang::withTrashed()->where($cabang->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'cabang'], function () {
	Route::get(	'', 		[CabangController::class, 'index']		)->name('cabangs.index');
    Route::get(	'create', 	[CabangController::class, 'create']	)->name('cabangs.create');
	Route::post('store', 	[CabangController::class, 'store']		)->name('cabangs.store');
    Route::get(	'deleted', 	[CabangController::class, 'deleted']	)->name('cabangs.deleted');
});

Route::group(['prefix' => 'cabang/{cabang}'], function () {
	// Cabang
	Route::get('/', [CabangController::class, 'show'])->name('cabangs.show');
	Route::get('edit', [CabangController::class, 'edit'])->name('cabangs.edit');
	Route::patch('update', [CabangController::class, 'update'])->name('cabangs.update');
	Route::delete('destroy', [CabangController::class, 'destroy'])->name('cabangs.destroy');
	// Deleted
	Route::get('restore', [CabangController::class, 'restore'])->name('cabangs.restore');
	Route::get('delete', [CabangController::class, 'delete'])->name('cabangs.delete-permanently');
});