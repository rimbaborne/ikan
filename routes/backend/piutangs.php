<?php

use App\Http\Controllers\Backend\PiutangController;

use App\Models\Piutang;

Route::bind('piutang', function ($value) {
	$piutang = new Piutang;

	return Piutang::withTrashed()->where($piutang->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'piutangs'], function () {
	Route::get(	'', 		[PiutangController::class, 'index']		)->name('piutangs.index');
    Route::get(	'create', 	[PiutangController::class, 'create']	)->name('piutangs.create');
	Route::post('store', 	[PiutangController::class, 'store']		)->name('piutangs.store');
    Route::get(	'deleted', 	[PiutangController::class, 'deleted']	)->name('piutangs.deleted');
});

Route::group(['prefix' => 'piutangs/{piutang}'], function () {
	// Piutang
	Route::get('/', [PiutangController::class, 'show'])->name('piutangs.show');
	Route::get('edit', [PiutangController::class, 'edit'])->name('piutangs.edit');
	Route::patch('update', [PiutangController::class, 'update'])->name('piutangs.update');
	Route::delete('destroy', [PiutangController::class, 'destroy'])->name('piutangs.destroy');
	// Deleted
	Route::get('restore', [PiutangController::class, 'restore'])->name('piutangs.restore');
	Route::get('delete', [PiutangController::class, 'delete'])->name('piutangs.delete-permanently');
});