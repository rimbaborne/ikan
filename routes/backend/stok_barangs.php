<?php

use App\Http\Controllers\Backend\StokBarangController;

use App\Models\StokBarang;

Route::bind('stok_barang', function ($value) {
	$stok_barang = new StokBarang;

	return StokBarang::withTrashed()->where($stok_barang->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'stok_barangs'], function () {
	Route::get(	'', 		[StokBarangController::class, 'index']		)->name('stok_barangs.index');
    Route::get(	'create', 	[StokBarangController::class, 'create']	)->name('stok_barangs.create');
	Route::post('store', 	[StokBarangController::class, 'store']		)->name('stok_barangs.store');
    Route::get(	'deleted', 	[StokBarangController::class, 'deleted']	)->name('stok_barangs.deleted');
});

Route::group(['prefix' => 'stok_barangs/{stok_barang}'], function () {
	// StokBarang
	Route::get('/', [StokBarangController::class, 'show'])->name('stok_barangs.show');
	Route::get('edit', [StokBarangController::class, 'edit'])->name('stok_barangs.edit');
	Route::patch('update', [StokBarangController::class, 'update'])->name('stok_barangs.update');
	Route::delete('destroy', [StokBarangController::class, 'destroy'])->name('stok_barangs.destroy');
	// Deleted
	Route::get('restore', [StokBarangController::class, 'restore'])->name('stok_barangs.restore');
	Route::get('delete', [StokBarangController::class, 'delete'])->name('stok_barangs.delete-permanently');
});