<?php

use App\Http\Controllers\Backend\JenisBarangController;

use App\Models\JenisBarang;

Route::bind('jenis_barang', function ($value) {
	$jenis_barang = new JenisBarang;

	return JenisBarang::withTrashed()->where($jenis_barang->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'jenis-barang'], function () {
	Route::get(	'', 		[JenisBarangController::class, 'index']		)->name('jenis_barangs.index');
    Route::get(	'create', 	[JenisBarangController::class, 'create']	)->name('jenis_barangs.create');
	Route::post('store', 	[JenisBarangController::class, 'store']		)->name('jenis_barangs.store');
    Route::get(	'deleted', 	[JenisBarangController::class, 'deleted']	)->name('jenis_barangs.deleted');
});

Route::group(['prefix' => 'jenis-barang/{jenis_barang}'], function () {
	// JenisBarang
	Route::get('/', [JenisBarangController::class, 'show'])->name('jenis_barangs.show');
	Route::get('edit', [JenisBarangController::class, 'edit'])->name('jenis_barangs.edit');
	Route::patch('update', [JenisBarangController::class, 'update'])->name('jenis_barangs.update');
	Route::delete('destroy', [JenisBarangController::class, 'destroy'])->name('jenis_barangs.destroy');
	// Deleted
	Route::get('restore', [JenisBarangController::class, 'restore'])->name('jenis_barangs.restore');
	Route::get('delete', [JenisBarangController::class, 'delete'])->name('jenis_barangs.delete-permanently');
});