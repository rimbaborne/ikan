<?php

use App\Http\Controllers\Backend\BarangController;

use App\Models\Barang;

Route::bind('barang', function ($value) {
	$barang = new Barang;

	return Barang::withTrashed()->where($barang->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'barang'], function () {
	Route::get(	'', 		[BarangController::class, 'index']		)->name('barangs.index');
    Route::get(	'create', 	[BarangController::class, 'create']	)->name('barangs.create');
	Route::post('store', 	[BarangController::class, 'store']		)->name('barangs.store');
    Route::get(	'deleted', 	[BarangController::class, 'deleted']	)->name('barangs.deleted');
});

Route::group(['prefix' => 'barang/{barang}'], function () {
	// Barang
	Route::get('/', [BarangController::class, 'show'])->name('barangs.show');
	Route::get('edit', [BarangController::class, 'edit'])->name('barangs.edit');
	Route::patch('update', [BarangController::class, 'update'])->name('barangs.update');
	Route::delete('destroy', [BarangController::class, 'destroy'])->name('barangs.destroy');
	// Deleted
	Route::get('restore', [BarangController::class, 'restore'])->name('barangs.restore');
	Route::get('delete', [BarangController::class, 'delete'])->name('barangs.delete-permanently');
});