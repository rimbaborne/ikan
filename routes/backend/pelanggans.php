<?php

use App\Http\Controllers\Backend\PelangganController;

use App\Models\Pelanggan;

Route::bind('pelanggan', function ($value) {
	$pelanggan = new Pelanggan;

	return Pelanggan::withTrashed()->where($pelanggan->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'pelanggan'], function () {
	Route::get(	'', 		[PelangganController::class, 'index']		)->name('pelanggans.index');
    Route::get(	'create', 	[PelangganController::class, 'create']	)->name('pelanggans.create');
	Route::post('store', 	[PelangganController::class, 'store']		)->name('pelanggans.store');
    Route::get(	'deleted', 	[PelangganController::class, 'deleted']	)->name('pelanggans.deleted');
});

Route::group(['prefix' => 'pelanggan/{pelanggan}'], function () {
	// Pelanggan
	Route::get('/', [PelangganController::class, 'show'])->name('pelanggans.show');
	Route::get('edit', [PelangganController::class, 'edit'])->name('pelanggans.edit');
	Route::patch('update', [PelangganController::class, 'update'])->name('pelanggans.update');
	Route::delete('destroy', [PelangganController::class, 'destroy'])->name('pelanggans.destroy');
	// Deleted
	Route::get('restore', [PelangganController::class, 'restore'])->name('pelanggans.restore');
	Route::get('delete', [PelangganController::class, 'delete'])->name('pelanggans.delete-permanently');
});