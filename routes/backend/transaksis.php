<?php

use App\Http\Controllers\Backend\TransaksiController;

use App\Models\Transaksi;

Route::bind('transaksi', function ($value) {
	$transaksi = new Transaksi;

	return Transaksi::withTrashed()->where($transaksi->getRouteKeyName(), $value)->first();
});

Route::group(['prefix' => 'transaksis'], function () {
	Route::get(	'', 		[TransaksiController::class, 'index']		)->name('transaksis.index');
    Route::get(	'create', 	[TransaksiController::class, 'create']	)->name('transaksis.create');
	Route::post('store', 	[TransaksiController::class, 'store']		)->name('transaksis.store');
    Route::get(	'deleted', 	[TransaksiController::class, 'deleted']	)->name('transaksis.deleted');
});

Route::group(['prefix' => 'transaksis/{transaksi}'], function () {
	// Transaksi
	Route::get('/', [TransaksiController::class, 'show'])->name('transaksis.show');
	Route::get('edit', [TransaksiController::class, 'edit'])->name('transaksis.edit');
	Route::patch('update', [TransaksiController::class, 'update'])->name('transaksis.update');
	Route::delete('destroy', [TransaksiController::class, 'destroy'])->name('transaksis.destroy');
	// Deleted
	Route::get('restore', [TransaksiController::class, 'restore'])->name('transaksis.restore');
	Route::get('delete', [TransaksiController::class, 'delete'])->name('transaksis.delete-permanently');
});