<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('nota')) {
            Schema::create('nota', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('id_pengguna');
                $table->unsignedBigInteger('id_user');
                $table->unsignedBigInteger('id_pelanggan');
                $table->unsignedBigInteger('id_transaksi');
                $table->string('kode_transaksi');
                $table->date('tanggal_nota');
                $table->unsignedBigInteger('total_bayar');
                $table->unsignedBigInteger('jumlah_uang_nota');
                $table->unsignedBigInteger('kembalian_nota');
                $table->string('status');
                $table->softDeletes();
                $table->timestamps();

                $table->foreign('id_pengguna')->references('id')->on('penggunas');
                $table->foreign('id_user')->references('id')->on('users');
                $table->foreign('id_pelanggan')->references('id')->on('pelanggans');
                $table->foreign('id_transaksi')->references('id')->on('transaksis');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nota');
    }
}
