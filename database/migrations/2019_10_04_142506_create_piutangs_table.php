<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePiutangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('piutangs')) {
            Schema::create('piutangs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('id_nota');
                $table->date('tanggal_bon');
                $table->unsignedBigInteger('jumlah_uang_bon');
                $table->unsignedBigInteger('kembalian_bon');
                $table->softDeletes();
                $table->timestamps();

                $table->foreign('id_nota')->references('id')->on('nota');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('piutangs');
    }
}
