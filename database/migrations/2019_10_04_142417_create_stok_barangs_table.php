<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStokBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('stok_barangs')) {
            Schema::create('stok_barangs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('id_barang');
                $table->unsignedBigInteger('id_cabang');
                $table->unsignedBigInteger('harga_selisih');
                $table->unsignedBigInteger('harga_cabang');
                $table->softDeletes();
                $table->timestamps();

                $table->foreign('id_barang')->references('id')->on('barangs');
                $table->foreign('id_cabang')->references('id')->on('cabangs');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stok_barangs');
    }
}
