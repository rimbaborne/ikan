<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('transaksis')) {
            Schema::create('transaksis', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('id_stok');
                $table->integer('kuantitas');
                $table->unsignedBigInteger('diskon');
                $table->unsignedBigInteger('total_harga');
                $table->unsignedBigInteger('no_urut');
                $table->softDeletes();
                $table->timestamps();

                $table->foreign('id_stok')->references('id')->on('stok_barangs');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
