<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('barangs')) {
            Schema::create('barangs', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('nama');
                $table->unsignedBigInteger('id_jenis');
                $table->unsignedBigInteger('harga');
                $table->softDeletes();
                $table->timestamps();

                $table->foreign('id_jenis')->references('id')->on('jenis_barangs');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barangs');
    }
}
