<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaryawansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('karyawans')) {
            Schema::create('karyawans', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('nama');
                $table->string('jenis_kelamin');
                $table->string('jabatan');
                $table->string('alamat');
                $table->string('telepon');
                $table->unsignedBigInteger('id_cabang');
                $table->unsignedBigInteger('id_user');
                $table->unsignedBigInteger('id_pengguna');
                $table->softDeletes();
                $table->timestamps();

                $table->foreign('id_cabang')->references('id')->on('cabangs');
                $table->foreign('id_pengguna')->references('id')->on('penggunas');
                $table->foreign('id_user')->references('id')->on('users');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karyawans');
    }
}
