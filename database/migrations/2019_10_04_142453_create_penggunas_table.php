<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenggunasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('penggunas')) {
            Schema::create('penggunas', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('username');
                $table->string('password');
                $table->string('level');
                $table->softDeletes();
                $table->timestamps();
                $table->rememberToken();
                
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penggunas');
    }
}
