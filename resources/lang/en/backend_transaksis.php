<?php

return [
    'table' => [
        'title'    => 'title',
        'created'       => 'Created',
        'actions'       => 'Actions',
        'last_updated'  => 'Updated',
        'total'         => 'Total|Totals',
        'deleted'       => 'Deleted',
    ],

    'alerts' => [
        'created' => 'New Transaksi created',
        'updated' => 'Transaksi updated',
        'deleted' => 'Transaksi was deleted',
        'deleted_permanently' => 'Transaksi was permanently deleted',
        'restored'  => 'Transaksi was restored',
    ],

    'labels'    => [
        'management'    => 'Management of Transaksi',
        'active'        => 'Active',
        'create'        => 'Create',
        'edit'          => 'Edit',
        'view'          => 'View',
        'title'    => 'title',
        'created_at'    => 'Created at',
        'last_updated'  => 'Updated at',
        'deleted'       => 'Deleted',
    ],

    'validation' => [
        'attributes' => [
            'title' => 'title',
        ]
    ],

    'sidebar' => [
        'title'  => 'Title',
    ],

    'tabs' => [
        'title'    => 'title',
        'content'   => [
            'overview' => [
                'title'    => 'title',
                'created_at'    => 'Created',
                'last_updated'  => 'Updated'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Transaksi',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];
