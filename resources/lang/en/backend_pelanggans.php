<?php

return [
    'table' => [
        'title'    => 'title',
        'created'       => 'Created',
        'actions'       => 'Actions',
        'last_updated'  => 'Updated',
        'total'         => 'Total|Totals',
        'deleted'       => 'Deleted',
    ],

    'alerts' => [
        'created' => 'New Pelanggan created',
        'updated' => 'Pelanggan updated',
        'deleted' => 'Pelanggan was deleted',
        'deleted_permanently' => 'Pelanggan was permanently deleted',
        'restored'  => 'Pelanggan was restored',
    ],

    'labels'    => [
        'management'    => 'Management of Pelanggan',
        'active'        => 'Active',
        'create'        => 'Create',
        'edit'          => 'Edit',
        'view'          => 'View',
        'title'    => 'title',
        'created_at'    => 'Created at',
        'last_updated'  => 'Updated at',
        'deleted'       => 'Deleted',
    ],

    'validation' => [
        'attributes' => [
            'title' => 'title',
        ]
    ],

    'sidebar' => [
        'title'  => 'Title',
    ],

    'tabs' => [
        'title'    => 'title',
        'content'   => [
            'overview' => [
                'title'    => 'title',
                'created_at'    => 'Created',
                'last_updated'  => 'Updated'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Pelanggan',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];
