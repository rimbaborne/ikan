<?php

return [
    'table' => [
        'title'    => 'Nama',
        'created'       => 'Dibuat',
        'actions'       => 'Aksi',
        'last_updated'  => 'Diperbaruhi',
        'total'         => 'Total|Totals',
        'deleted'       => 'Dihapus',
    ],

    'alerts' => [
        'created' => 'Nota Berhasil Dibuat',
        'updated' => 'Nota Diperbaruhi',
        'deleted' => 'Nota Telah Dihapus',
        'deleted_permanently' => 'Nota Telah Dihapus Permanen',
        'restored'  => 'Nota Telah Dipulihkan',
    ],

    'labels'    => [
        'management'    => 'Manajemen Nota',
        'active'        => 'Aktif',
        'create'        => 'Dibuat',
        'edit'          => 'Edit',
        'view'          => 'Lihat',
        'title'         => 'Nama',
        'created_at'    => 'Dibuat Pada',
        'last_updated'  => 'Diperbaruhi Pada',
        'deleted'       => 'Dihapus',
    ],

    'validation' => [
        'attributes' => [
            'title' => 'Nama',
        ]
    ],

    'sidebar' => [
        'title'  => 'Nama',
    ],

    'tabs' => [
        'title'    => 'Nama',
        'content'   => [
            'overview' => [
                'title'    => 'Nama',
                'created_at'    => 'Dibuat Pada',
                'last_updated'  => 'Terakhir Diperbaruhi'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Nota',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];
