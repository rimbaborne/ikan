<?php

return [
    'table' => [
        'title'    => 'Nama',
        'created'       => 'Dibuat',
        'actions'       => 'Aksi',
        'last_updated'  => 'Diperbaruhi',
        'total'         => 'Total|Totals',
        'deleted'       => 'Dihapus',
    ],

    'alerts' => [
        'created' => 'Barang Berhasil Dibuat',
        'updated' => 'Barang Diperbaruhi',
        'deleted' => 'Barang Telah Dihapus',
        'deleted_permanently' => 'Barang Telah Dihapus Permanen',
        'restored'  => 'Barang Telah Dipulihkan',
    ],

    'labels'    => [
        'management'    => 'Manajemen Barang',
        'active'        => 'Aktif',
        'create'        => 'Dibuat',
        'edit'          => 'Edit',
        'view'          => 'Lihat',
        'title'         => 'Nama',
        'created_at'    => 'Dibuat Pada',
        'last_updated'  => 'Diperbaruhi Pada',
        'deleted'       => 'Dihapus',
    ],

    'validation' => [
        'attributes' => [
            'title' => 'Nama',
        ]
    ],

    'sidebar' => [
        'title'  => 'Nama',
    ],

    'tabs' => [
        'title'    => 'Nama',
        'content'   => [
            'overview' => [
                'title'    => 'Nama',
                'created_at'    => 'Dibuat Pada',
                'last_updated'  => 'Terakhir Diperbaruhi'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Barang',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];
