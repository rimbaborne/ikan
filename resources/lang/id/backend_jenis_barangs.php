<?php

return [
    'table' => [
        'title'    => 'Nama',
        'created'       => 'Dibuat',
        'actions'       => 'Aksi',
        'last_updated'  => 'Diperbaruhi',
        'total'         => 'Total|Totals',
        'deleted'       => 'Dihapus',
    ],

    'alerts' => [
        'created' => 'Jenis Barang Berhasil Dibuat',
        'updated' => 'Jenis Barang Diperbaruhi',
        'deleted' => 'Jenis Barang Telah Dihapus',
        'deleted_permanently' => 'Jenis Barang Telah Dihapus Permanen',
        'restored'  => 'Jenis Barang Telah Dipulihkan',
    ],

    'labels'    => [
        'management'    => 'Manajemen Jenis Barang',
        'active'        => 'Aktif',
        'create'        => 'Dibuat',
        'edit'          => 'Edit',
        'view'          => 'Lihat',
        'title'         => 'Nama',
        'created_at'    => 'Dibuat Pada',
        'last_updated'  => 'Diperbaruhi Pada',
        'deleted'       => 'Dihapus',
    ],

    'validation' => [
        'attributes' => [
            'title' => 'nama',
        ]
    ],

    'sidebar' => [
        'title'  => 'Nama',
    ],

    'tabs' => [
        'title'    => 'nama',
        'content'   => [
            'overview' => [
                'title'    => 'nama',
                'created_at'    => 'Dibuat Pada',
                'last_updated'  => 'Terakhir Diperbaruhi'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Jenis Barang',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];
