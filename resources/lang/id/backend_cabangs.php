<?php

return [
    'table' => [
        'title'    => 'Nama',
        'created'       => 'Dibuat',
        'actions'       => 'Aksi',
        'last_updated'  => 'Diperbaruhi',
        'total'         => 'Total|Totals',
        'deleted'       => 'Dihapus',
    ],

    'alerts' => [
        'created' => 'Cabang Berhasil Dibuat',
        'updated' => 'Cabang Diperbaruhi',
        'deleted' => 'Cabang Telah Dihapus',
        'deleted_permanently' => 'Cabang Telah Dihapus Permanen',
        'restored'  => 'Cabang Telah Dipulihkan',
    ],

    'labels'    => [
        'management'    => 'Manajemen Cabang',
        'active'        => 'Aktif',
        'create'        => 'Dibuat',
        'edit'          => 'Edit',
        'view'          => 'Lihat',
        'title'         => 'Nama',
        'created_at'    => 'Dibuat Pada',
        'last_updated'  => 'Diperbaruhi Pada',
        'deleted'       => 'Dihapus',
    ],

    'validation' => [
        'attributes' => [
            'title' => 'Nama',
        ]
    ],

    'sidebar' => [
        'title'  => 'Nama',
    ],

    'tabs' => [
        'title'    => 'Nama',
        'content'   => [
            'overview' => [
                'title'    => 'Nama',
                'created_at'    => 'Dibuat Pada',
                'last_updated'  => 'Terakhir Diperbaruhi'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Cabang',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];
