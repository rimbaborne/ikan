<?php

return [
    'table' => [
        'title'    => 'Nama',
        'created'       => 'Dibuat',
        'actions'       => 'Aksi',
        'last_updated'  => 'Diperbaruhi',
        'total'         => 'Total|Totals',
        'deleted'       => 'Dihapus',
    ],

    'alerts' => [
        'created' => 'Transaksi Berhasil Dibuat',
        'updated' => 'Transaksi Diperbaruhi',
        'deleted' => 'Transaksi Telah Dihapus',
        'deleted_permanently' => 'Transaksi Telah Dihapus Permanen',
        'restored'  => 'Transaksi Telah Dipulihkan',
    ],

    'labels'    => [
        'management'    => 'Manajemen Transaksi',
        'active'        => 'Aktif',
        'create'        => 'Dibuat',
        'edit'          => 'Edit',
        'view'          => 'Lihat',
        'title'         => 'Nama',
        'created_at'    => 'Dibuat Pada',
        'last_updated'  => 'Diperbaruhi Pada',
        'deleted'       => 'Dihapus',
    ],

    'validation' => [
        'attributes' => [
            'title' => 'Nama',
        ]
    ],

    'sidebar' => [
        'title'  => 'Nama',
    ],

    'tabs' => [
        'title'    => 'Nama',
        'content'   => [
            'overview' => [
                'title'    => 'Nama',
                'created_at'    => 'Dibuat Pada',
                'last_updated'  => 'Terakhir Diperbaruhi'
            ],
        ],
    ],

    'menus' => [
      'main' => 'Transaksi',
      'all' => 'All',
      'create' => 'Create',
      'deleted' => 'Deleted'
    ]
];
