@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_cabangs.labels.management'))

@section('breadcrumb-links')
    @include('backend.cabang.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_cabangs.labels.management') }} <small class="text-muted">{{ __('backend_cabangs.labels.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.cabang.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>@lang('backend_cabangs.table.title')</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Pimpinan</th>
                            <th>@lang('backend_cabangs.table.created')</th>
                            <th>@lang('backend_cabangs.table.last_updated')</th>
                            <th>@lang('backend_cabangs.table.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                        $no = 1;
                        @endphp
                        @foreach($cabangs as $cabang)
                            <tr>
                                <td class="align-middle">{{ $no }}</td>
                                <td class="align-middle"><a href="/admin/cabang/{{ $cabang->id }}">{{ $cabang->nama }}</a></td>
                                <td class="align-middle">{{ $cabang->alamat }}</td>
                                <td class="align-middle">{{ $cabang->telepon }}</td>
                                <td class="align-middle">{{ $cabang->pimpinan }}</td>
                                <td class="align-middle">{!! $cabang->created_at !!}</td>
                                <td class="align-middle">{{ $cabang->updated_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $cabang->action_buttons !!}</td>
                            </tr>
                        @php
                        $no++;
                        @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $cabangs->count() !!} {{ trans_choice('backend_cabangs.table.total', $cabangs->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $cabangs->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
