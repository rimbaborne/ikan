@extends('backend.layouts.app')

@section('title', __('backend_cabangs.labels.management') . ' | ' . __('backend_cabangs.labels.view'))

@section('breadcrumb-links')
    @include('backend.cabang.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('backend_cabangs.labels.management')
                    <small class="text-muted">@lang('backend_cabangs.labels.view')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fas fa-user"></i> 
                            Detail
                        </a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">

                        <div class="col">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th width="300">@lang('backend_cabangs.tabs.content.overview.title')</th>
                                        <td>: {{ $cabang->nama }}</td>
                                    </tr>
                                    <tr>
                                        <th width="300">Alamat</th>
                                        <td>: {{ $cabang->alamat }}</td>
                                    </tr>
                                    <tr>
                                        <th width="300">Telepon</th>
                                        <td>: {{ $cabang->telepon }}</td>
                                    </tr>
                                    <tr>
                                        <th width="300">Nama Pimpinan</th>
                                        <td>: {{ $cabang->pimpinan }}</td>
                                    </tr>
                                </table>
                            </div><!--table-responsive-->
                        </div><!--col-->

                    </div><!--tab-->
                </div><!--tab-content-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>@lang('backend_cabangs.tabs.content.overview.created_at'):</strong> {{ timezone()->convertToLocal($cabang->created_at) }} ({{ $cabang->created_at->diffForHumans() }}),
                    <strong>@lang('backend_cabangs.tabs.content.overview.last_updated'):</strong> {{ timezone()->convertToLocal($cabang->updated_at) }} ({{ $cabang->updated_at->diffForHumans() }})
                    @if($cabang->trashed())
                        <strong>@lang('backend_cabangs.tabs.content.overview.deleted_at'):</strong> {{ timezone()->convertToLocal($cabang->deleted_at) }} ({{ $cabang->deleted_at->diffForHumans() }})
                    @endif
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection
