@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_cabangs.labels.management'))

@section('breadcrumb-links')
    @include('backend.cabang.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_cabangs.labels.management') }} <small class="text-muted">{{ __('backend_cabangs.labels.deleted') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.cabang.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_cabangs.table.title')</th>
                            <th>@lang('backend_cabangs.table.created')</th>
                            <th>@lang('backend_cabangs.table.deleted')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cabangs as $cabang)
                            <tr>
                                <td class="align-middle"><a href="/admin/cabangs/{{ $cabang->id }}">{{ $cabang->nama }}</a></td>
                                <td class="align-middle">{!! $cabang->created_at !!}</td>
                                <td class="align-middle">{{ $cabang->deleted_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $cabang->trashed_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $cabangs->count() !!} {{ trans_choice('backend_cabangs.table.total', $cabangs->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $cabangs->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
