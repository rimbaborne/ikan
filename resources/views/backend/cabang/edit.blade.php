@extends('backend.layouts.app')

@section('title', __('backend_cabangs.labels.management') . ' | ' . __('backend_cabangs.labels.edit'))

@section('breadcrumb-links')
    @include('backend.cabang.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($cabang, 'PATCH', route('admin.cabangs.update', $cabang->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('backend_cabangs.labels.management')
                        <small class="text-muted">@lang('backend_cabangs.labels.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('backend_cabangs.validation.attributes.title'))->class('col-md-2 col-form-label')->for('nama') }}

                        <div class="col-md-10">
                            {{ html()->text('nama')
                                ->class('form-control')
                                ->placeholder(__('backend_cabangs.validation.attributes.title'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label('Alamat')->class('col-md-2 col-form-label')->for('alamat') }}

                        <div class="col-md-10">
                            {{ html()->text('alamat')
                                ->class('form-control')
                                ->placeholder('Alamat')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label('Telepon')->class('col-md-2 col-form-label')->for('telepon') }}

                        <div class="col-md-10">
                            {{ html()->text('telepon')
                                ->class('form-control')
                                ->placeholder('Telepon')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label('Pimpinan')->class('col-md-2 col-form-label')->for('pimpinan') }}

                        <div class="col-md-10">
                            {{ html()->text('pimpinan')
                                ->class('form-control')
                                ->placeholder('Nama Pimpinan')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.cabangs.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
