<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/cabangs*')) }}" href="{{ route('admin.cabangs.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_cabangs.sidebar.title')
    </a>
</li>