<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_cabangs.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.cabangs.index') }}">@lang('backend_cabangs.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.cabangs.create') }}">@lang('backend_cabangs.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.cabangs.deactivated') }}">@lang('backed_cabangs.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.cabangs.deleted') }}">@lang('backend_cabangs.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
