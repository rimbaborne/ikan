@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_jenis_barangs.labels.management'))

@section('breadcrumb-links')
    @include('backend.jenis_barang.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{-- {{ __('backend_jenis_barangs.labels.management') }}  --}}
                    Daftar Jenis Barang 
                    <small class="text-muted">{{ __('backend_jenis_barangs.labels.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.jenis_barang.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            {{-- <th>@lang('backend_jenis_barangs.table.title')</th> --}}
                            <th>No</th>
                            <th>Nama Jenis</th>
                            <th>@lang('backend_jenis_barangs.table.created')</th>
                            <th>@lang('backend_jenis_barangs.table.last_updated')</th>
                            <th>@lang('backend_jenis_barangs.table.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                        $no = 1;
                        @endphp
                        @foreach($jenisBarangs as $jenisBarang)
                            <tr>
                                <td class="align-middle">{{ $no }}</td> 
                                <td class="align-middle"><a href="/admin/jenis-barang/{{ $jenisBarang->id }}">{{ $jenisBarang->nama }}</a></td>
                                <td class="align-middle">{!! $jenisBarang->created_at !!}</td>
                                <td class="align-middle">{{ $jenisBarang->updated_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $jenisBarang->action_buttons !!}</td>
                            </tr>
                        @php
                        $no++;
                        @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $jenisBarangs->count() !!} {{ trans_choice('backend_jenis_barangs.table.total', $jenisBarangs->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $jenisBarangs->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
