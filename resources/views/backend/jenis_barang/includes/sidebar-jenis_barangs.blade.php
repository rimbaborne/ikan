<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/jenis-barang*')) }}" href="{{ route('admin.jenis_barangs.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_jenis_barangs.sidebar.title')
    </a>
</li>