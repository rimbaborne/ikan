<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_jenis_barangs.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.jenis_barangs.index') }}">@lang('backend_jenis_barangs.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.jenis_barangs.create') }}">@lang('backend_jenis_barangs.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.jenis_barangs.deactivated') }}">@lang('backed_jenis_barangs.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.jenis_barangs.deleted') }}">@lang('backend_jenis_barangs.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
