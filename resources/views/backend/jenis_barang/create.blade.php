@extends('backend.layouts.app')

@section('title', __('backend_jenis_barangs.labels.management') . ' | ' . __('backend_jenis_barangs.labels.create'))

@section('breadcrumb-links')
    @include('backend.jenis_barang.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.jenis_barangs.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{-- @lang('backend_jenis_barangs.labels.management') --}}
                        Input Jenis Barang 
                        <small class="text-muted">@lang('backend_jenis_barangs.labels.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    {{ html()->label('Nama Jenis Barang')->class('col-md-2 col-form-label float-right')->for('nama') }}

                        <div class="col-md-10">
                            {{ html()->text('nama')
                                ->class('form-control')
                                ->placeholder('Jenis Barang')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.jenis_barangs.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
