@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_jenis_barangs.labels.management'))

@section('breadcrumb-links')
    @include('backend.jenis_barang.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{-- {{ __('backend_jenis_barangs.labels.management') }}  --}}
                    Jenis Barang
                    <small class="text-muted">{{ __('backend_jenis_barangs.labels.deleted') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.jenis_barang.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>
                                {{-- @lang('backend_jenis_barangs.table.title') --}}
                                Nama
                            </th>
                            <th>@lang('backend_jenis_barangs.table.created')</th>
                            <th>@lang('backend_jenis_barangs.table.deleted')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($jenisBarangs as $jenisBarang)
                            <tr>
                                <td class="align-middle"><a href="/admin/jenis-barang/{{ $jenisBarang->id }}">{{ $jenisBarang->nama }}</a></td>
                                <td class="align-middle">{!! $jenisBarang->created_at !!}</td>
                                <td class="align-middle">{{ $jenisBarang->deleted_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $jenisBarang->trashed_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $jenisBarangs->count() !!} {{ trans_choice('backend_jenis_barangs.table.total', $jenisBarangs->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $jenisBarangs->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
