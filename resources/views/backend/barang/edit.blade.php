@extends('backend.layouts.app')

@section('title', __('backend_barangs.labels.management') . ' | ' . __('backend_barangs.labels.edit'))

@section('breadcrumb-links')
    @include('backend.barang.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->modelForm($barang, 'PATCH', route('admin.barangs.update', $barang->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('backend_barangs.labels.management')
                        <small class="text-muted">@lang('backend_barangs.labels.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('backend_barangs.validation.attributes.title'))->class('col-md-2 col-form-label')->for('nama') }}

                        <div class="col-md-10">
                            {{ html()->text('nama')
                                ->class('form-control')
                                ->placeholder(__('backend_barangs.validation.attributes.title'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                    {{ html()->label('Jenis Barang')->class('col-md-2 col-form-label')->for('id_jenis') }}
                        <div class="col-md-10">
                            {{ html()->text('id_jenis')
                                ->class('form-control')
                                ->placeholder('Jenis Barang')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                    {{ html()->label('Harga Barang')->class('col-md-2 col-form-label')->for('harga') }}
                        <div class="col-md-10">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <strong>Rp.</strong>
                                    </span>
                                </div>
                                {{ html()->text('harga')
                                    ->class('form-control')
                                    ->placeholder('Jenis Barang')
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!-- /.input-group -->
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.barangs.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
