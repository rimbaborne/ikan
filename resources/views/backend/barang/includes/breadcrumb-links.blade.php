<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_barangs.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.barangs.index') }}">@lang('backend_barangs.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.barangs.create') }}">@lang('backend_barangs.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.barangs.deactivated') }}">@lang('backed_barangs.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.barangs.deleted') }}">@lang('backend_barangs.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
