<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/barang*')) }}" href="{{ route('admin.barangs.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_barangs.sidebar.title')
    </a>
</li>