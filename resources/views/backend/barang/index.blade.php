@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_barangs.labels.management'))

@section('breadcrumb-links')
    @include('backend.barang.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_barangs.labels.management') }} <small class="text-muted">{{ __('backend_barangs.labels.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.barang.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>@lang('backend_barangs.table.title')</th>
                            <th>Jenis</th>
                            <th>Harga</th>
                            <th>@lang('backend_barangs.table.created')</th>
                            <th>@lang('backend_barangs.table.last_updated')</th>
                            <th>@lang('backend_barangs.table.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                        $no = 1;
                        @endphp
                        @foreach($barangs as $barang)
                            <tr>
                                <td class="align-middle">{{ $no }}</td> 
                                <td class="align-middle"><a href="/admin/barang/{{ $barang->id }}">{{ $barang->nama }}</a></td>
                                <td class="align-middle">{{ $barang->id_jenis }}</td>
                                <td class="align-middle">@currency($barang->harga)</td>
                                <td class="align-middle">{!! $barang->created_at !!}</td>
                                <td class="align-middle">{{ $barang->updated_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $barang->action_buttons !!}</td>
                            </tr>
                        @php
                        $no++;
                        @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $barangs->count() !!} {{ trans_choice('backend_barangs.table.total', $barangs->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $barangs->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
