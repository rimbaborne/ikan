<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/dashboard'))
                }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            <li class="nav-title">
                Menu
            </li>

            {{-- Link Pelanggan --}}
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/karyawan*'))
                }}" href="{{ route('admin.karyawans.index') }}">
                    <i class="nav-icon fas fa-users"></i>
                    Karyawan
                </a>
            </li>
            {{-- Link Pelanggan --}}
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/cabang*'))
                }}" href="{{ route('admin.cabangs.index') }}">
                    <i class="nav-icon fas fa-building"></i>
                    Cabang
                </a>
            </li>
            {{-- Link Pelanggan --}}
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/pelanggan*'))
                }}" href="{{ route('admin.pelanggans.index') }}">
                    <i class="nav-icon fas fa-user"></i>
                    Pelanggan
                </a>
            </li>
            {{-- Link Barang --}}
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/barang*'))
                }}" href="{{ route('admin.barangs.index') }}">
                    <i class="nav-icon fas fa-server"></i>
                    Barang
                </a>
            </li>
            {{-- Link Jenis Barang --}}
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/jenis-barang*'))
                }}" href="{{ route('admin.jenis_barangs.index') }}">
                    <i class="nav-icon fas fa-list"></i>
                    Jenis Barang
                </a>
            </li>

            <li class="nav-title">
                @lang('menus.backend.sidebar.system')
            </li>

            @if ($logged_in_user->isAdmin())
                <li class="nav-item nav-dropdown {{
                    active_class(Active::checkUriPattern('admin/auth*'), 'open')
                }}">
                    <a class="nav-link nav-dropdown-toggle {{
                        active_class(Active::checkUriPattern('admin/auth*'))
                    }}" href="#">
                        <i class="nav-icon far fa-user"></i>
                        @lang('menus.backend.access.title')

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Active::checkUriPattern('admin/auth/user*'))
                            }}" href="{{ route('admin.auth.user.index') }}">
                                @lang('labels.backend.access.users.management')

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Active::checkUriPattern('admin/auth/role*'))
                            }}" href="{{ route('admin.auth.role.index') }}">
                                @lang('labels.backend.access.roles.management')
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="divider"></li>

                <li class="nav-item nav-dropdown {{
                    active_class(Active::checkUriPattern('admin/log-viewer*'), 'open')
                }}">
                        <a class="nav-link nav-dropdown-toggle {{
                            active_class(Active::checkUriPattern('admin/log-viewer*'))
                        }}" href="#">
                        <i class="nav-icon fas fa-list"></i> @lang('menus.backend.log-viewer.main')
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Active::checkUriPattern('admin/log-viewer'))
                        }}" href="{{ route('log-viewer::dashboard') }}">
                                @lang('menus.backend.log-viewer.dashboard')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Active::checkUriPattern('admin/log-viewer/logs*'))
                        }}" href="{{ route('log-viewer::logs.list') }}">
                                @lang('menus.backend.log-viewer.logs')
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
