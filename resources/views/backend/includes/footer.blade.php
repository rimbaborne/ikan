<footer class="app-footer">
    <div>
        <strong>
            @lang('labels.general.copyright') &copy; {{ date('Y') }}
        </strong> 
        {{-- @lang('strings.backend.general.all_rights_reserved') --}}
    </div>

    <div class="ml-auto">Develope by <a href="#">Tukang Kode</a></div>
</footer>
