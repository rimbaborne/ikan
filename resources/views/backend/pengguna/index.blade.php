@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_penggunas.labels.management'))

@section('breadcrumb-links')
    @include('backend.pengguna.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_penggunas.labels.management') }} <small class="text-muted">{{ __('backend_penggunas.labels.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.pengguna.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_penggunas.table.title')</th>
                            <th>@lang('backend_penggunas.table.created')</th>
                            <th>@lang('backend_penggunas.table.last_updated')</th>
                            <th>@lang('backend_penggunas.table.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($penggunas as $pengguna)
                            <tr>
                                <td class="align-middle"><a href="/admin/penggunas/{{ $pengguna->id }}">{{ $pengguna->title }}</a></td>
                                <td class="align-middle">{!! $pengguna->created_at !!}</td>
                                <td class="align-middle">{{ $pengguna->updated_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $pengguna->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $penggunas->count() !!} {{ trans_choice('backend_penggunas.table.total', $penggunas->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $penggunas->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
