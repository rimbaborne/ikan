<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/penggunas*')) }}" href="{{ route('admin.penggunas.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_penggunas.sidebar.title')
    </a>
</li>