<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_penggunas.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.penggunas.index') }}">@lang('backend_penggunas.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.penggunas.create') }}">@lang('backend_penggunas.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.penggunas.deactivated') }}">@lang('backed_penggunas.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.penggunas.deleted') }}">@lang('backend_penggunas.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
