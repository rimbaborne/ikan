@extends('backend.layouts.app')

@section('title', __('backend_karyawans.labels.management') . ' | ' . __('backend_karyawans.labels.view'))

@section('breadcrumb-links')
    @include('backend.karyawan.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    @lang('backend_karyawans.labels.management')
                    <small class="text-muted">@lang('backend_karyawans.labels.view')</small>
                </h4>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4 mb-4">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-expanded="true"><i class="fas fa-user"></i> Detail</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="overview" role="tabpanel" aria-expanded="true">

                        <div class="col">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <tr>
                                        <th>@lang('backend_karyawans.tabs.content.overview.title')</th>
                                        <td>: {{ $karyawan->nama}}</td>
                                    </tr>
                                    <tr>
                                        <th width="300">Jabatan</th>
                                        <td>: {{ $karyawan->jabatan }}</td>
                                    </tr>
                                    <tr>
                                        <th width="300">Jenis Kelamin</th>
                                        <td>: {{ $karyawan->jenis_kelamin }}</td>
                                    </tr>
                                    <tr>
                                        <th width="300">Alamat</th>
                                        <td>: {{ $karyawan->alamat }}</td>
                                    </tr>
                                    <tr>
                                        <th width="300">Telepon</th>
                                        <td>: {{ $karyawan->telepon }}</td>
                                    </tr>
                                    <tr>
                                        <th width="300">Cabang</th>
                                        <td>: {{ $karyawan->id_cabang }}</td>
                                    </tr>
                                    <tr>
                                        <th width="300">Akun</th>
                                        <td>: {{ $karyawan->id_user }}</td>
                                    </tr>
                                </table>
                            </div><!--table-responsive-->
                        </div><!--col-->

                    </div><!--tab-->
                </div><!--tab-content-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->

    <div class="card-footer">
        <div class="row">
            <div class="col">
                <small class="float-right text-muted">
                    <strong>@lang('backend_karyawans.tabs.content.overview.created_at'):</strong> {{ timezone()->convertToLocal($karyawan->created_at) }} ({{ $karyawan->created_at->diffForHumans() }}),
                    <strong>@lang('backend_karyawans.tabs.content.overview.last_updated'):</strong> {{ timezone()->convertToLocal($karyawan->updated_at) }} ({{ $karyawan->updated_at->diffForHumans() }})
                    @if($karyawan->trashed())
                        <strong>@lang('backend_karyawans.tabs.content.overview.deleted_at'):</strong> {{ timezone()->convertToLocal($karyawan->deleted_at) }} ({{ $karyawan->deleted_at->diffForHumans() }})
                    @endif
                </small>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
</div><!--card-->
@endsection
