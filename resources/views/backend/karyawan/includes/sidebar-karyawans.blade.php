<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/karyawans*')) }}" href="{{ route('admin.karyawans.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_karyawans.sidebar.title')
    </a>
</li>