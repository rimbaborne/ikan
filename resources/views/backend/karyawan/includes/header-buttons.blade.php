<div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
    <a href="{{ route('admin.karyawans.create') }}" class="btn btn-success ml-1"  title="@lang('labels.backend.karyawans.create_new')"><i class="fas fa-plus-circle"></i></a>
</div><!--btn-toolbar-->
