<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_karyawans.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.karyawans.index') }}">@lang('backend_karyawans.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.karyawans.create') }}">@lang('backend_karyawans.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.karyawans.deactivated') }}">@lang('backed_karyawans.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.karyawans.deleted') }}">@lang('backend_karyawans.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
