@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_karyawans.labels.management'))

@section('breadcrumb-links')
    @include('backend.karyawan.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_karyawans.labels.management') }} <small class="text-muted">{{ __('backend_karyawans.labels.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.karyawan.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>@lang('backend_karyawans.table.title')</th>
                            <th>Jabatan</th>
                            <th>Jenis Kelamin</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Cabang</th>
                            <th>Akun</th>
                            <th>@lang('backend_karyawans.table.created')</th>
                            {{-- <th>@lang('backend_karyawans.table.last_updated')</th> --}}
                            <th>@lang('backend_karyawans.table.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                        $no = 1;
                        @endphp
                        @foreach($karyawans as $karyawan)
                            <tr>
                                <td class="align-middle">{{ $no }}</td> 
                                <td class="align-middle"><a href="/admin/karyawan/{{ $karyawan->id }}">{{ $karyawan->nama }}</a></td>
                                <td class="align-middle">{{ $karyawan->jabatan }}</td>
                                <td class="align-middle">{{ $karyawan->jenis_kelamin }}</td>
                                <td class="align-middle">{{ $karyawan->alamat }}</td>
                                <td class="align-middle">{{ $karyawan->telepon }}</td>
                                <td class="align-middle">{{ $karyawan->id_cabang }}</td>
                                <td class="align-middle">{{ $karyawan->id_user }}</td>
                                <td class="align-middle">{!! $karyawan->created_at !!}</td>
                                {{-- <td class="align-middle">{{ $karyawan->updated_at->diffForHumans() }}</td> --}}
                                <td class="align-middle">{!! $karyawan->action_buttons !!}</td>
                            </tr>
                        @php
                        $no++;
                        @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $karyawans->count() !!} {{ trans_choice('backend_karyawans.table.total', $karyawans->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $karyawans->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
