@extends('backend.layouts.app')

@section('title', __('backend_karyawans.labels.management') . ' | ' . __('backend_karyawans.labels.create'))

@section('breadcrumb-links')
    @include('backend.karyawan.includes.breadcrumb-links')
@endsection

@section('content')
{{ html()->form('POST', route('admin.karyawans.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('backend_karyawans.labels.management')
                        <small class="text-muted">@lang('backend_karyawans.labels.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                    {{ html()->label(__('backend_karyawans.validation.attributes.title'))->class('col-md-2 col-form-label')->for('nama') }}
                        <div class="col-md-10">
                            {{ html()->text('nama')
                                ->class('form-control')
                                ->placeholder(__('backend_karyawans.validation.attributes.title'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label('Jenis Kelamin')->class('col-md-2 col-form-label')->for('nama') }}
                        <div class="col-md-10">
                            <div class="form-check form-check-inline mr-1">
                                <input class="form-check-input" type="radio" value="Laki-laki" name="jenis_kelamin">
                                <label class="form-check-label">Laki-laki</label>
                            </div>
                            <div class="form-check form-check-inline mr-1">
                                <input class="form-check-input" type="radio" value="Perempuan" name="jenis_kelamin">
                                <label class="form-check-label">Perempuan</label>
                            </div>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label('Jabatan')->class('col-md-2 col-form-label')->for('jabatan') }}
                        <div class="col-md-10">
                            {{ html()->text('jabatan')
                                ->class('form-control')
                                ->placeholder('Jabatan')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label('Alamat')->class('col-md-2 col-form-label')->for('alamat') }}
                        <div class="col-md-10">
                            {{ html()->text('alamat')
                                ->class('form-control')
                                ->placeholder('Alamat')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label('Telepon')->class('col-md-2 col-form-label')->for('telepon') }}
                        <div class="col-md-10">
                            {{ html()->text('telepon')
                                ->class('form-control')
                                ->placeholder('Telepon')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label('id_cabang')->class('col-md-2 col-form-label')->for('id_cabang') }}
                        <div class="col-md-10">
                            {{ html()->text('id_cabang')
                                ->class('form-control')
                                ->placeholder('id_cabang')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label('id_user')->class('col-md-2 col-form-label')->for('id_user') }}
                        <div class="col-md-10">
                            {{ html()->text('id_user')
                                ->class('form-control')
                                ->placeholder('id_user')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label('id_pengguna')->class('col-md-2 col-form-label')->for('id_pengguna') }}
                        <div class="col-md-10">
                            {{ html()->text('id_pengguna')
                                ->class('form-control')
                                ->placeholder('id_pengguna')
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.karyawans.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
{{ html()->closeModelForm() }}
@endsection
