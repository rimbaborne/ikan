<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_piutangs.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.piutangs.index') }}">@lang('backend_piutangs.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.piutangs.create') }}">@lang('backend_piutangs.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.piutangs.deactivated') }}">@lang('backed_piutangs.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.piutangs.deleted') }}">@lang('backend_piutangs.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
