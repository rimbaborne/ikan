<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/piutangs*')) }}" href="{{ route('admin.piutangs.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_piutangs.sidebar.title')
    </a>
</li>