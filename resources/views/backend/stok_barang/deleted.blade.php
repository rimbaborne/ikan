@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_stok_barangs.labels.management'))

@section('breadcrumb-links')
    @include('backend.stok_barang.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_stok_barangs.labels.management') }} <small class="text-muted">{{ __('backend_stok_barangs.labels.deleted') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.stok_barang.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_stok_barangs.table.title')</th>
                            <th>@lang('backend_stok_barangs.table.created')</th>
                            <th>@lang('backend_stok_barangs.table.deleted')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($stokBarangs as $stokBarang)
                            <tr>
                                <td class="align-middle"><a href="/admin/stok_barangs/{{ $stokBarang->id }}">{{ $stokBarang->title }}</a></td>
                                <td class="align-middle">{!! $stokBarang->created_at !!}</td>
                                <td class="align-middle">{{ $stokBarang->deleted_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $stokBarang->trashed_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $stokBarangs->count() !!} {{ trans_choice('backend_stok_barangs.table.total', $stokBarangs->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $stokBarangs->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
