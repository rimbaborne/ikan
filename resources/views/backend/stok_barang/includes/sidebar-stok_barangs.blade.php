<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/stok_barangs*')) }}" href="{{ route('admin.stok_barangs.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_stok_barangs.sidebar.title')
    </a>
</li>