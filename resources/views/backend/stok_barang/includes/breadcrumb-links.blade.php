<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_stok_barangs.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.stok_barangs.index') }}">@lang('backend_stok_barangs.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.stok_barangs.create') }}">@lang('backend_stok_barangs.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.stok_barangs.deactivated') }}">@lang('backed_stok_barangs.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.stok_barangs.deleted') }}">@lang('backend_stok_barangs.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
