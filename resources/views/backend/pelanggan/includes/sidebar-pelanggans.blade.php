<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/pelanggans*')) }}" href="{{ route('admin.pelanggans.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_pelanggans.sidebar.title')
    </a>
</li>