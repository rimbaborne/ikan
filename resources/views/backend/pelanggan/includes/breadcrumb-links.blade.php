<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_pelanggans.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.pelanggans.index') }}">@lang('backend_pelanggans.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.pelanggans.create') }}">@lang('backend_pelanggans.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.pelanggans.deactivated') }}">@lang('backed_pelanggans.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.pelanggans.deleted') }}">@lang('backend_pelanggans.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
