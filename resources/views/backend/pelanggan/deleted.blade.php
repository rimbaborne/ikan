@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_pelanggans.labels.management'))

@section('breadcrumb-links')
    @include('backend.pelanggan.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_pelanggans.labels.management') }} <small class="text-muted">{{ __('backend_pelanggans.labels.deleted') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.pelanggan.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_pelanggans.table.title')</th>
                            <th>@lang('backend_pelanggans.table.created')</th>
                            <th>@lang('backend_pelanggans.table.deleted')</th>
                            <th>@lang('labels.general.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pelanggans as $pelanggan)
                            <tr>
                                <td class="align-middle"><a href="/admin/pelanggan/{{ $pelanggan->id }}">{{ $pelanggan->nama }}</a></td>
                                <td class="align-middle">{!! $pelanggan->created_at !!}</td>
                                <td class="align-middle">{{ $pelanggan->deleted_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $pelanggan->trashed_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $pelanggans->count() !!} {{ trans_choice('backend_pelanggans.table.total', $pelanggans->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $pelanggans->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
