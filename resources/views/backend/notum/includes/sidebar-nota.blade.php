<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/nota*')) }}" href="{{ route('admin.nota.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_nota.sidebar.title')
    </a>
</li>