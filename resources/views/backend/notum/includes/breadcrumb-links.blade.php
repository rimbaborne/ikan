<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_nota.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.nota.index') }}">@lang('backend_nota.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.nota.create') }}">@lang('backend_nota.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.nota.deactivated') }}">@lang('backed_nota.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.nota.deleted') }}">@lang('backend_nota.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
