<li class="breadcrumb-menu">
    <div class="btn-group" role="group" aria-label="Button group">
        <div class="dropdown">
            <a class="btn dropdown-toggle" href="#" role="button" id="breadcrumb-dropdown-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">@lang('backend_transaksis.menus.main')</a>

            <div class="dropdown-menu" aria-labelledby="breadcrumb-dropdown-1">
                <a class="dropdown-item" href="{{ route('admin.transaksis.index') }}">@lang('backend_transaksis.menus.all')</a>
                <a class="dropdown-item" href="{{ route('admin.transaksis.create') }}">@lang('backend_transaksis.menus.create')</a>
                {{-- <a class="dropdown-item" href="{{ route('admin.transaksis.deactivated') }}">@lang('backed_transaksis.menus.deactivated')</a> --}}
                <a class="dropdown-item" href="{{ route('admin.transaksis.deleted') }}">@lang('backend_transaksis.menus.deleted')</a>
            </div>
        </div><!--dropdown-->

        <!--<a class="btn" href="#">Static Link</a>-->
    </div><!--btn-group-->
</li>
