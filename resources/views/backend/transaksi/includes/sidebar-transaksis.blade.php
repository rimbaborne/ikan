<li class="nav-item">
    <a class="nav-link {{ active_class(Active::checkUriPattern('admin/transaksis*')) }}" href="{{ route('admin.transaksis.index') }}">
        <i class="nav-icon icon-folder"></i> @lang('backend_transaksis.sidebar.title')
    </a>
</li>