@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('backend_transaksis.labels.management'))

@section('breadcrumb-links')
    @include('backend.transaksi.includes.breadcrumb-links')
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    {{ __('backend_transaksis.labels.management') }} <small class="text-muted">{{ __('backend_transaksis.labels.active') }}</small>
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
                @include('backend.transaksi.includes.header-buttons')
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('backend_transaksis.table.title')</th>
                            <th>@lang('backend_transaksis.table.created')</th>
                            <th>@lang('backend_transaksis.table.last_updated')</th>
                            <th>@lang('backend_transaksis.table.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($transaksis as $transaksi)
                            <tr>
                                <td class="align-middle"><a href="/admin/transaksis/{{ $transaksi->id }}">{{ $transaksi->title }}</a></td>
                                <td class="align-middle">{!! $transaksi->created_at !!}</td>
                                <td class="align-middle">{{ $transaksi->updated_at->diffForHumans() }}</td>
                                <td class="align-middle">{!! $transaksi->action_buttons !!}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                    {!! $transaksis->count() !!} {{ trans_choice('backend_transaksis.table.total', $transaksis->count()) }}
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                    {!! $transaksis->links() !!}
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
