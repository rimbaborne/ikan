@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.frontend.dashboard') )

@section('content')
    <div class="row mb-4">
        <div class="col">
            <!--<div class="card">
                <div class="card-header">
                    <strong>
                        <i class="fas fa-tachometer-alt"></i> @lang('navs.frontend.dashboard')
                    </strong>
                </div>card-header-->

                {{-- <div class="card-body"> --}}
                    <div class="row">
                        <div class="col col-sm-8 order-1 order-sm-2  mb-4">
                            <div class="row">
                                <div class="col">
                                    <div class="card mb-4">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label for="Email1">No. Nota</label>
                                                    <input type="email" class="form-control" placeholder="C01K.1909.0001" disabled>
                                                </div><!-- /.col-sm-6 -->
                                                <div class="col-sm-6">
                                                    <label for="Email1">No. Nota</label>
                                                    <input type="email" class="form-control" placeholder="C01K.1909.0001" disabled>
                                                </div><!-- /.col-sm-6 -->
                                            </div><!-- /.row -->
                                            <div class="row" style="padding-top: 15px; padding-bottom:7px;">
                                                <div class="col-sm-12">
                                                    <label for="Alamat">Alamat</label>
                                                    <textarea name="alamat" class="form-control" id="" rows="5" disabled></textarea> 
                                                </div><!-- /.col-sm-6 -->
                                            </div><!-- /.row -->
                                        </div><!--card-body-->
                                    </div><!--card-->
                                </div><!--col-md-6-->
                            </div><!--row-->
                        </div><!--col-md-4-->

                        {{-- <img class="card-img-top" src="{{ $logged_in_user->picture }}" alt="Profile Picture"> --}}

                        <div class="col-md-4 order-2 order-sm-1">
                            <div class="row">
                                <div class="col">
                                    <div class="card mb-4">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="Email1">No. Nota</label>
                                                <input type="email" class="form-control" placeholder="C01K.1909.0001" disabled>
                                            </div><!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="Email1">Tanggal</label>
                                                <input type="email" class="form-control" placeholder="19 / 9 / 2019" disabled>
                                            </div><!-- /.form-group -->
                                            <div class="form-group">
                                                <label for="Email1">Kasir</label>
                                                <input type="email" class="form-control" placeholder="John Doe" disabled>
                                            </div><!-- /.form-group -->
                                        </div><!--card-body-->
                                    </div><!--card-->
                                </div><!--col-md-6-->
                            </div><!--row-->
                        </div><!--col-md-8-->
                    </div><!-- row -->

                    <div class="row">
                        <div class="col">
                            <div class="card mb-4">
                                    <table class="table" style="margin-bottom:0">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col"><center>NO</center></th>
                                                <th width="130" scope="col"><center>KODE</center></th>
                                                <th width="350" scope="col"><center>NAMA BARANG</center></th>
                                                <th scope="col"><center>HARGA</center></th>
                                                <th scope="col"><center>QTY(Kg)</center></th>
                                                <th scope="col"><center>JUMLAH</center></th>
                                                <th width="10" scope="col"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">1</th>
                                                <td>A1.001</td>
                                                <td>Ikan Tenggiri</td>
                                                <td>Rp. 15.000,-</td>
                                                <td><center>100</center></td>
                                                <td>Rp. 1.500.000,-</td>
                                                <td><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">2</th>
                                                <td>A2.011</td>
                                                <td>Ikan Nila</td>
                                                <td>Rp. 20.000,-</td>
                                                <td><center>50</center></td>
                                                <td>Rp. 1.000.000,-</td>
                                                <td><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">3</th>
                                                <td>A3.021</td>
                                                <td>Ikan Gurame</td>
                                                <td>Rp. 25.000,-</td>
                                                <td><center>20</center></td>
                                                <td>Rp. 500.000,-</td>
                                                <td><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                            </tr>
                                            <tr>
                                                <th scope="row">4</th>
                                                <td>A4.021</td>
                                                <td>Cumi-cumi</td>
                                                <td>Rp. 30.000,-</td>
                                                <td><center>50</center></td>
                                                <td>Rp. 1.500.000,-</td>
                                                <td><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                            </tr>
                                            <tr>
                                                    <th scope="row"></th>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>SUB TOTAL</td>
                                                    <td>Rp. 5.000.000,-</td>
                                                    <td><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button></td>
                                                </tr>
                                        </tbody>
                                        <tfoot class="thead-dark">
                                            <tr>
                                                <th scope="col"><button class="btn btn-sm btn-light"><i class="fa fa-plus" aria-hidden="true"></i></button></th>
                                                <th width="120" scope="col">Tambah Item</th>
                                                <th width="350" scope="col">
                                                    <div style="padding-left: 55%;">
                                                        <div class="input-group pull-right">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text">Diskon %</span>
                                                            </div>
                                                            <input type="text" class="form-control" style="flex:0.8;margin:0" value="10">
                                                        </div>
                                                    </div>
                                                </th>
                                                <th scope="col"></th>
                                                <th scope="col" class="pull-right"><div class="pull-right">TOTAL</div></th>
                                                <th scope="col">RP. 4.500.000</th>
                                                <th width="10" scope="col"></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                            </div><!--card-->
                        </div><!--col-md-6-->
                    </div><!--row-->

               <!-- </div>  card-body -->
           <!-- </div> card -->
        </div><!-- row -->
    </div><!-- row -->
@endsection
