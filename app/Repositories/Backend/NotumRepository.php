<?php

namespace App\Repositories\Backend;

use App\Models\Notum;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class NotumRepository.
 */
class NotumRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Notum::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Notum
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Notum
    {
        return DB::transaction(function () use ($data) {
            $notum = parent::create([
                'title' => $data['title'],
            ]);

            if ($notum) {
                return $notum;
            }

            throw new GeneralException(__('backend_nota.exceptions.create_error'));
        });
    }

    /**
     * @param Notum  $notum
     * @param array     $data
     *
     * @return Notum
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Notum $notum, array $data) : Notum
    {
        return DB::transaction(function () use ($notum, $data) {
            if ($notum->update([
                'title' => $data['title'],
            ])) {

                return $notum;
            }

            throw new GeneralException(__('backend_nota.exceptions.update_error'));
        });
    }

    /**
     * @param Notum $notum
     *
     * @return Notum
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Notum $notum) : Notum
    {
        if (is_null($notum->deleted_at)) {
            throw new GeneralException(__('backend_nota.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($notum) {
            if ($notum->forceDelete()) {
                return $notum;
            }

            throw new GeneralException(__('backend_nota.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Notum $notum
     *
     * @return Notum
     * @throws GeneralException
     */
    public function restore(Notum $notum) : Notum
    {
        if (is_null($notum->deleted_at)) {
            throw new GeneralException(__('backend_nota.exceptions.cant_restore'));
        }

        if ($notum->restore()) {
            return $notum;
        }

        throw new GeneralException(__('backend_nota.exceptions.restore_error'));
    }
}
