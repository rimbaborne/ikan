<?php

namespace App\Repositories\Backend;

use App\Models\Piutang;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class PiutangRepository.
 */
class PiutangRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Piutang::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Piutang
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Piutang
    {
        return DB::transaction(function () use ($data) {
            $piutang = parent::create([
                'title' => $data['title'],
            ]);

            if ($piutang) {
                return $piutang;
            }

            throw new GeneralException(__('backend_piutangs.exceptions.create_error'));
        });
    }

    /**
     * @param Piutang  $piutang
     * @param array     $data
     *
     * @return Piutang
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Piutang $piutang, array $data) : Piutang
    {
        return DB::transaction(function () use ($piutang, $data) {
            if ($piutang->update([
                'title' => $data['title'],
            ])) {

                return $piutang;
            }

            throw new GeneralException(__('backend_piutangs.exceptions.update_error'));
        });
    }

    /**
     * @param Piutang $piutang
     *
     * @return Piutang
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Piutang $piutang) : Piutang
    {
        if (is_null($piutang->deleted_at)) {
            throw new GeneralException(__('backend_piutangs.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($piutang) {
            if ($piutang->forceDelete()) {
                return $piutang;
            }

            throw new GeneralException(__('backend_piutangs.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Piutang $piutang
     *
     * @return Piutang
     * @throws GeneralException
     */
    public function restore(Piutang $piutang) : Piutang
    {
        if (is_null($piutang->deleted_at)) {
            throw new GeneralException(__('backend_piutangs.exceptions.cant_restore'));
        }

        if ($piutang->restore()) {
            return $piutang;
        }

        throw new GeneralException(__('backend_piutangs.exceptions.restore_error'));
    }
}
