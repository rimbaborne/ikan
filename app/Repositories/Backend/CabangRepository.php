<?php

namespace App\Repositories\Backend;

use App\Models\Cabang;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class CabangRepository.
 */
class CabangRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Cabang::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Cabang
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Cabang
    {
        return DB::transaction(function () use ($data) {
            $cabang = parent::create([
                'nama' => $data['nama'],
                'alamat' => $data['alamat'],
                'telepon' => $data['telepon'],
                'pimpinan' => $data['pimpinan'],
            ]);

            if ($cabang) {
                return $cabang;
            }

            throw new GeneralException(__('backend_cabangs.exceptions.create_error'));
        });
    }

    /**
     * @param Cabang  $cabang
     * @param array     $data
     *
     * @return Cabang
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Cabang $cabang, array $data) : Cabang
    {
        return DB::transaction(function () use ($cabang, $data) {
            if ($cabang->update([
                'nama' => $data['nama'],
                'alamat' => $data['alamat'],
                'telepon' => $data['telepon'],
                'pimpinan' => $data['pimpinan'],
            ])) {

                return $cabang;
            }

            throw new GeneralException(__('backend_cabangs.exceptions.update_error'));
        });
    }

    /**
     * @param Cabang $cabang
     *
     * @return Cabang
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Cabang $cabang) : Cabang
    {
        if (is_null($cabang->deleted_at)) {
            throw new GeneralException(__('backend_cabangs.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($cabang) {
            if ($cabang->forceDelete()) {
                return $cabang;
            }

            throw new GeneralException(__('backend_cabangs.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Cabang $cabang
     *
     * @return Cabang
     * @throws GeneralException
     */
    public function restore(Cabang $cabang) : Cabang
    {
        if (is_null($cabang->deleted_at)) {
            throw new GeneralException(__('backend_cabangs.exceptions.cant_restore'));
        }

        if ($cabang->restore()) {
            return $cabang;
        }

        throw new GeneralException(__('backend_cabangs.exceptions.restore_error'));
    }
}
