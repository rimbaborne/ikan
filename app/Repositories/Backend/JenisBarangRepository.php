<?php

namespace App\Repositories\Backend;

use App\Models\JenisBarang;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class JenisBarangRepository.
 */
class JenisBarangRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return JenisBarang::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return JenisBarang
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : JenisBarang
    {
        return DB::transaction(function () use ($data) {
            $jenis_barang = parent::create([
                'nama' => $data['nama'],
            ]);

            if ($jenis_barang) {
                return $jenis_barang;
            }

            throw new GeneralException(__('backend_jenis_barangs.exceptions.create_error'));
        });
    }

    /**
     * @param JenisBarang  $jenis_barang
     * @param array     $data
     *
     * @return JenisBarang
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(JenisBarang $jenis_barang, array $data) : JenisBarang
    {
        return DB::transaction(function () use ($jenis_barang, $data) {
            if ($jenis_barang->update([
                'nama' => $data['nama'],
            ])) {

                return $jenis_barang;
            }

            throw new GeneralException(__('backend_jenis_barangs.exceptions.update_error'));
        });
    }

    /**
     * @param JenisBarang $jenis_barang
     *
     * @return JenisBarang
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(JenisBarang $jenis_barang) : JenisBarang
    {
        if (is_null($jenis_barang->deleted_at)) {
            throw new GeneralException(__('backend_jenis_barangs.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($jenis_barang) {
            if ($jenis_barang->forceDelete()) {
                return $jenis_barang;
            }

            throw new GeneralException(__('backend_jenis_barangs.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param JenisBarang $jenis_barang
     *
     * @return JenisBarang
     * @throws GeneralException
     */
    public function restore(JenisBarang $jenis_barang) : JenisBarang
    {
        if (is_null($jenis_barang->deleted_at)) {
            throw new GeneralException(__('backend_jenis_barangs.exceptions.cant_restore'));
        }

        if ($jenis_barang->restore()) {
            return $jenis_barang;
        }

        throw new GeneralException(__('backend_jenis_barangs.exceptions.restore_error'));
    }
}
