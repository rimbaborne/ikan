<?php

namespace App\Repositories\Backend;

use App\Models\Barang;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class BarangRepository.
 */
class BarangRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Barang::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Barang
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Barang
    {
        return DB::transaction(function () use ($data) {
            $barang = parent::create([
                'nama' => $data['nama'],
                'id_jenis' => $data['id_jenis'],
                'harga' => $data['harga'],
            ]);

            if ($barang) {
                return $barang;
            }

            throw new GeneralException(__('backend_barangs.exceptions.create_error'));
        });
    }

    /**
     * @param Barang  $barang
     * @param array     $data
     *
     * @return Barang
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Barang $barang, array $data) : Barang
    {
        return DB::transaction(function () use ($barang, $data) {
            if ($barang->update([
                'nama' => $data['nama'],
                'id_jenis' => $data['id_jenis'],
                'harga' => $data['harga'],
            ])) {

                return $barang;
            }

            throw new GeneralException(__('backend_barangs.exceptions.update_error'));
        });
    }

    /**
     * @param Barang $barang
     *
     * @return Barang
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Barang $barang) : Barang
    {
        if (is_null($barang->deleted_at)) {
            throw new GeneralException(__('backend_barangs.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($barang) {
            if ($barang->forceDelete()) {
                return $barang;
            }

            throw new GeneralException(__('backend_barangs.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Barang $barang
     *
     * @return Barang
     * @throws GeneralException
     */
    public function restore(Barang $barang) : Barang
    {
        if (is_null($barang->deleted_at)) {
            throw new GeneralException(__('backend_barangs.exceptions.cant_restore'));
        }

        if ($barang->restore()) {
            return $barang;
        }

        throw new GeneralException(__('backend_barangs.exceptions.restore_error'));
    }
}
