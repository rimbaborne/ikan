<?php

namespace App\Repositories\Backend;

use App\Models\Pengguna;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class PenggunaRepository.
 */
class PenggunaRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Pengguna::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Pengguna
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Pengguna
    {
        return DB::transaction(function () use ($data) {
            $pengguna = parent::create([
                'title' => $data['title'],
            ]);

            if ($pengguna) {
                return $pengguna;
            }

            throw new GeneralException(__('backend_penggunas.exceptions.create_error'));
        });
    }

    /**
     * @param Pengguna  $pengguna
     * @param array     $data
     *
     * @return Pengguna
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Pengguna $pengguna, array $data) : Pengguna
    {
        return DB::transaction(function () use ($pengguna, $data) {
            if ($pengguna->update([
                'title' => $data['title'],
            ])) {

                return $pengguna;
            }

            throw new GeneralException(__('backend_penggunas.exceptions.update_error'));
        });
    }

    /**
     * @param Pengguna $pengguna
     *
     * @return Pengguna
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Pengguna $pengguna) : Pengguna
    {
        if (is_null($pengguna->deleted_at)) {
            throw new GeneralException(__('backend_penggunas.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($pengguna) {
            if ($pengguna->forceDelete()) {
                return $pengguna;
            }

            throw new GeneralException(__('backend_penggunas.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Pengguna $pengguna
     *
     * @return Pengguna
     * @throws GeneralException
     */
    public function restore(Pengguna $pengguna) : Pengguna
    {
        if (is_null($pengguna->deleted_at)) {
            throw new GeneralException(__('backend_penggunas.exceptions.cant_restore'));
        }

        if ($pengguna->restore()) {
            return $pengguna;
        }

        throw new GeneralException(__('backend_penggunas.exceptions.restore_error'));
    }
}
