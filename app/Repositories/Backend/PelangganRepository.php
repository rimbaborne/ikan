<?php

namespace App\Repositories\Backend;

use App\Models\Pelanggan;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class PelangganRepository.
 */
class PelangganRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Pelanggan::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Pelanggan
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Pelanggan
    {
        return DB::transaction(function () use ($data) {
            $pelanggan = parent::create([
                'nama' => $data['nama'],
                'alamat' => $data['alamat'],
                'telepon' => $data['telepon'],
            ]);

            if ($pelanggan) {
                return $pelanggan;
            }

            throw new GeneralException(__('backend_pelanggans.exceptions.create_error'));
        });
    }

    /**
     * @param Pelanggan  $pelanggan
     * @param array     $data
     *
     * @return Pelanggan
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Pelanggan $pelanggan, array $data) : Pelanggan
    {
        return DB::transaction(function () use ($pelanggan, $data) {
            if ($pelanggan->update([
                'nama' => $data['nama'],
                'alamat' => $data['alamat'],
                'telepon' => $data['telepon'],
            ])) {

                return $pelanggan;
            }

            throw new GeneralException(__('backend_pelanggans.exceptions.update_error'));
        });
    }

    /**
     * @param Pelanggan $pelanggan
     *
     * @return Pelanggan
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Pelanggan $pelanggan) : Pelanggan
    {
        if (is_null($pelanggan->deleted_at)) {
            throw new GeneralException(__('backend_pelanggans.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($pelanggan) {
            if ($pelanggan->forceDelete()) {
                return $pelanggan;
            }

            throw new GeneralException(__('backend_pelanggans.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Pelanggan $pelanggan
     *
     * @return Pelanggan
     * @throws GeneralException
     */
    public function restore(Pelanggan $pelanggan) : Pelanggan
    {
        if (is_null($pelanggan->deleted_at)) {
            throw new GeneralException(__('backend_pelanggans.exceptions.cant_restore'));
        }

        if ($pelanggan->restore()) {
            return $pelanggan;
        }

        throw new GeneralException(__('backend_pelanggans.exceptions.restore_error'));
    }
}
