<?php

namespace App\Repositories\Backend;

use App\Models\StokBarang;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class StokBarangRepository.
 */
class StokBarangRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return StokBarang::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return StokBarang
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : StokBarang
    {
        return DB::transaction(function () use ($data) {
            $stok_barang = parent::create([
                'title' => $data['title'],
            ]);

            if ($stok_barang) {
                return $stok_barang;
            }

            throw new GeneralException(__('backend_stok_barangs.exceptions.create_error'));
        });
    }

    /**
     * @param StokBarang  $stok_barang
     * @param array     $data
     *
     * @return StokBarang
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(StokBarang $stok_barang, array $data) : StokBarang
    {
        return DB::transaction(function () use ($stok_barang, $data) {
            if ($stok_barang->update([
                'title' => $data['title'],
            ])) {

                return $stok_barang;
            }

            throw new GeneralException(__('backend_stok_barangs.exceptions.update_error'));
        });
    }

    /**
     * @param StokBarang $stok_barang
     *
     * @return StokBarang
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(StokBarang $stok_barang) : StokBarang
    {
        if (is_null($stok_barang->deleted_at)) {
            throw new GeneralException(__('backend_stok_barangs.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($stok_barang) {
            if ($stok_barang->forceDelete()) {
                return $stok_barang;
            }

            throw new GeneralException(__('backend_stok_barangs.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param StokBarang $stok_barang
     *
     * @return StokBarang
     * @throws GeneralException
     */
    public function restore(StokBarang $stok_barang) : StokBarang
    {
        if (is_null($stok_barang->deleted_at)) {
            throw new GeneralException(__('backend_stok_barangs.exceptions.cant_restore'));
        }

        if ($stok_barang->restore()) {
            return $stok_barang;
        }

        throw new GeneralException(__('backend_stok_barangs.exceptions.restore_error'));
    }
}
