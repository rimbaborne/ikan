<?php

namespace App\Repositories\Backend;

use App\Models\Karyawan;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class KaryawanRepository.
 */
class KaryawanRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Karyawan::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Karyawan
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Karyawan
    {
        return DB::transaction(function () use ($data) {
            $karyawan = parent::create([
                'nama' => $data['nama'],
                'alamat' => $data['alamat'],
                'telepon' => $data['telepon'],
                'jenis_kelamin' => $data['jenis_kelamin'],
                'id_cabang' => $data['id_cabang'],
                'id_user' => $data['id_user'],
                'id_pengguna' => $data['id_pengguna'],
            ]);

            if ($karyawan) {
                return $karyawan;
            }

            throw new GeneralException(__('backend_karyawans.exceptions.create_error'));
        });
    }

    /**
     * @param Karyawan  $karyawan
     * @param array     $data
     *
     * @return Karyawan
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Karyawan $karyawan, array $data) : Karyawan
    {
        return DB::transaction(function () use ($karyawan, $data) {
            if ($karyawan->update([
                'nama' => $data['nama'],
                'alamat' => $data['alamat'],
                'telepon' => $data['telepon'],
                'jenis_kelamin' => $data['jenis_kelamin'],
                'id_cabang' => $data['id_cabang'],                
            ])) {

                return $karyawan;
            }

            throw new GeneralException(__('backend_karyawans.exceptions.update_error'));
        });
    }

    /**
     * @param Karyawan $karyawan
     *
     * @return Karyawan
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Karyawan $karyawan) : Karyawan
    {
        if (is_null($karyawan->deleted_at)) {
            throw new GeneralException(__('backend_karyawans.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($karyawan) {
            if ($karyawan->forceDelete()) {
                return $karyawan;
            }

            throw new GeneralException(__('backend_karyawans.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Karyawan $karyawan
     *
     * @return Karyawan
     * @throws GeneralException
     */
    public function restore(Karyawan $karyawan) : Karyawan
    {
        if (is_null($karyawan->deleted_at)) {
            throw new GeneralException(__('backend_karyawans.exceptions.cant_restore'));
        }

        if ($karyawan->restore()) {
            return $karyawan;
        }

        throw new GeneralException(__('backend_karyawans.exceptions.restore_error'));
    }
}
