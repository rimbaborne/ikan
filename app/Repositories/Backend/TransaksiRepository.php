<?php

namespace App\Repositories\Backend;

use App\Models\Transaksi;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class TransaksiRepository.
 */
class TransaksiRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Transaksi::class;
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return mixed
     */
    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param int    $paged
     * @param string $orderBy
     * @param string $sort
     *
     * @return LengthAwarePaginator
     */
    public function getDeletedPaginated($paged = 25, $orderBy = 'created_at', $sort = 'desc') : LengthAwarePaginator
    {
        return $this->model
            ->onlyTrashed()
            ->orderBy($orderBy, $sort)
            ->paginate($paged);
    }

    /**
     * @param array $data
     *
     * @return Transaksi
     * @throws \Exception
     * @throws \Throwable
     */
    public function create(array $data) : Transaksi
    {
        return DB::transaction(function () use ($data) {
            $transaksi = parent::create([
                'title' => $data['title'],
            ]);

            if ($transaksi) {
                return $transaksi;
            }

            throw new GeneralException(__('backend_transaksis.exceptions.create_error'));
        });
    }

    /**
     * @param Transaksi  $transaksi
     * @param array     $data
     *
     * @return Transaksi
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(Transaksi $transaksi, array $data) : Transaksi
    {
        return DB::transaction(function () use ($transaksi, $data) {
            if ($transaksi->update([
                'title' => $data['title'],
            ])) {

                return $transaksi;
            }

            throw new GeneralException(__('backend_transaksis.exceptions.update_error'));
        });
    }

    /**
     * @param Transaksi $transaksi
     *
     * @return Transaksi
     * @throws GeneralException
     * @throws \Exception
     * @throws \Throwable
     */
    public function forceDelete(Transaksi $transaksi) : Transaksi
    {
        if (is_null($transaksi->deleted_at)) {
            throw new GeneralException(__('backend_transaksis.exceptions.delete_first'));
        }

        return DB::transaction(function () use ($transaksi) {
            if ($transaksi->forceDelete()) {
                return $transaksi;
            }

            throw new GeneralException(__('backend_transaksis.exceptions.delete_error'));
        });
    }

    /**
     * Restore the specified soft deleted resource.
     *
     * @param Transaksi $transaksi
     *
     * @return Transaksi
     * @throws GeneralException
     */
    public function restore(Transaksi $transaksi) : Transaksi
    {
        if (is_null($transaksi->deleted_at)) {
            throw new GeneralException(__('backend_transaksis.exceptions.cant_restore'));
        }

        if ($transaksi->restore()) {
            return $transaksi;
        }

        throw new GeneralException(__('backend_transaksis.exceptions.restore_error'));
    }
}
