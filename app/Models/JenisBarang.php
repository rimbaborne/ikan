<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Traits\Attribute\JenisBarangAttribute;

class JenisBarang extends Model
{
    use JenisBarangAttribute,
        SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama',
    ];

    // protected $table = 'barangs';

    // public function barang()
    // {
    //     return $this->hasMany('App\Models\Barang','id_jenis');
    // }
}
