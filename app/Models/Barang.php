<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Traits\Attribute\BarangAttribute;

class Barang extends Model
{
    use BarangAttribute,
        SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama','id_jenis','harga'
    ];

    // protected $table = 'jenis_barangs';

    // public function jenisbarang()
    // {   
    //     return $this->belongsTo('App\Models\JenisBarang');
    // }

//     public function id_jenis() { 
//         return $this->belongsTo('App\Models\JenisBarang', 'id');
//     }
}