<?php

namespace App\Listeners\Backend\Pelanggan;

/**
 * Class PelangganEventListener.
 */
class PelangganEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->pelanggan->nama;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->pelanggan->nama;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->pelanggan->nama;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Pelanggan\PelangganCreated::class,
            'App\Listeners\Backend\Pelanggan\PelangganEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Pelanggan\PelangganUpdated::class,
            'App\Listeners\Backend\Pelanggan\PelangganEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Pelanggan\PelangganDeleted::class,
            'App\Listeners\Backend\Pelanggan\PelangganEventListener@onDeleted'
        );
    }
}
