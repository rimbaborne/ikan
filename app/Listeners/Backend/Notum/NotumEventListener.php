<?php

namespace App\Listeners\Backend\Notum;

/**
 * Class NotumEventListener.
 */
class NotumEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->notum->title;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->notum->title;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->notum->title;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Notum\NotumCreated::class,
            'App\Listeners\Backend\Notum\NotumEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Notum\NotumUpdated::class,
            'App\Listeners\Backend\Notum\NotumEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Notum\NotumDeleted::class,
            'App\Listeners\Backend\Notum\NotumEventListener@onDeleted'
        );
    }
}
