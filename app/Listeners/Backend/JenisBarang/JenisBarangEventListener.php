<?php

namespace App\Listeners\Backend\JenisBarang;

/**
 * Class JenisBarangEventListener.
 */
class JenisBarangEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->jenis_barang->nama;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->jenis_barang->nama;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->jenis_barang->nama;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\JenisBarang\JenisBarangCreated::class,
            'App\Listeners\Backend\JenisBarang\JenisBarangEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\JenisBarang\JenisBarangUpdated::class,
            'App\Listeners\Backend\JenisBarang\JenisBarangEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\JenisBarang\JenisBarangDeleted::class,
            'App\Listeners\Backend\JenisBarang\JenisBarangEventListener@onDeleted'
        );
    }
}
