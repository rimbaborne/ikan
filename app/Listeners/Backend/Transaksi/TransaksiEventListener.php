<?php

namespace App\Listeners\Backend\Transaksi;

/**
 * Class TransaksiEventListener.
 */
class TransaksiEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->transaksi->title;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->transaksi->title;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->transaksi->title;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Transaksi\TransaksiCreated::class,
            'App\Listeners\Backend\Transaksi\TransaksiEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Transaksi\TransaksiUpdated::class,
            'App\Listeners\Backend\Transaksi\TransaksiEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Transaksi\TransaksiDeleted::class,
            'App\Listeners\Backend\Transaksi\TransaksiEventListener@onDeleted'
        );
    }
}
