<?php

namespace App\Listeners\Backend\Karyawan;

/**
 * Class KaryawanEventListener.
 */
class KaryawanEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->karyawan->name;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->karyawan->name;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->karyawan->name;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Karyawan\KaryawanCreated::class,
            'App\Listeners\Backend\Karyawan\KaryawanEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Karyawan\KaryawanUpdated::class,
            'App\Listeners\Backend\Karyawan\KaryawanEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Karyawan\KaryawanDeleted::class,
            'App\Listeners\Backend\Karyawan\KaryawanEventListener@onDeleted'
        );
    }
}
