<?php

namespace App\Listeners\Backend\Pengguna;

/**
 * Class PenggunaEventListener.
 */
class PenggunaEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->pengguna->title;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->pengguna->title;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->pengguna->title;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Pengguna\PenggunaCreated::class,
            'App\Listeners\Backend\Pengguna\PenggunaEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Pengguna\PenggunaUpdated::class,
            'App\Listeners\Backend\Pengguna\PenggunaEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Pengguna\PenggunaDeleted::class,
            'App\Listeners\Backend\Pengguna\PenggunaEventListener@onDeleted'
        );
    }
}
