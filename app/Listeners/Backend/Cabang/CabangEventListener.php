<?php

namespace App\Listeners\Backend\Cabang;

/**
 * Class CabangEventListener.
 */
class CabangEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->cabang->nama;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->cabang->nama;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->cabang->nama;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Cabang\CabangCreated::class,
            'App\Listeners\Backend\Cabang\CabangEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Cabang\CabangUpdated::class,
            'App\Listeners\Backend\Cabang\CabangEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Cabang\CabangDeleted::class,
            'App\Listeners\Backend\Cabang\CabangEventListener@onDeleted'
        );
    }
}
