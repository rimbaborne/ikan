<?php

namespace App\Listeners\Backend\Piutang;

/**
 * Class PiutangEventListener.
 */
class PiutangEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->piutang->title;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->piutang->title;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->piutang->title;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Piutang\PiutangCreated::class,
            'App\Listeners\Backend\Piutang\PiutangEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Piutang\PiutangUpdated::class,
            'App\Listeners\Backend\Piutang\PiutangEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Piutang\PiutangDeleted::class,
            'App\Listeners\Backend\Piutang\PiutangEventListener@onDeleted'
        );
    }
}
