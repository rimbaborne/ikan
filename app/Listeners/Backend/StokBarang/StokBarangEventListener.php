<?php

namespace App\Listeners\Backend\StokBarang;

/**
 * Class StokBarangEventListener.
 */
class StokBarangEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        $user    = auth()->user()->name;

        $newitem = $event->stok_barang->title;

        \Log::info('User ' . $user . ' has created item ' . $newitem);
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        $user           = auth()->user()->name;

        $updated_item   = $event->stok_barang->title;

        \Log::info('User ' . $user . ' has updated item ' . $updated_item);
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        $user           = auth()->user()->name;

        $deleted_item   = $event->stok_barang->title;

        \Log::info('User ' . $user . ' has deleted item ' . $deleted_item);    }


    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\StokBarang\StokBarangCreated::class,
            'App\Listeners\Backend\StokBarang\StokBarangEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\StokBarang\StokBarangUpdated::class,
            'App\Listeners\Backend\StokBarang\StokBarangEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\StokBarang\StokBarangDeleted::class,
            'App\Listeners\Backend\StokBarang\StokBarangEventListener@onDeleted'
        );
    }
}
