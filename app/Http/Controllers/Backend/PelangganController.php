<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Pelanggan;
use App\Repositories\Backend\PelangganRepository;
use App\Http\Requests\Backend\Pelanggan\ManagePelangganRequest;
use App\Http\Requests\Backend\Pelanggan\StorePelangganRequest;
use App\Http\Requests\Backend\Pelanggan\UpdatePelangganRequest;

use App\Events\Backend\Pelanggan\PelangganCreated;
use App\Events\Backend\Pelanggan\PelangganUpdated;
use App\Events\Backend\Pelanggan\PelangganDeleted;

class PelangganController extends Controller
{
    /**
     * @var PelangganRepository
     */
    protected $pelangganRepository;

    /**
     * PelangganController constructor.
     *
     * @param PelangganRepository $pelangganRepository
     */
    public function __construct(PelangganRepository $pelangganRepository)
    {
        $this->pelangganRepository = $pelangganRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManagePelangganRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManagePelangganRequest $request)
    {
        return view('backend.pelanggan.index')
            ->withpelanggans($this->pelangganRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManagePelangganRequest    $request
     *
     * @return mixed
     */
    public function create(ManagePelangganRequest $request)
    {
        return view('backend.pelanggan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePelangganRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StorePelangganRequest $request)
    {
        $this->pelangganRepository->create($request->only(
            'nama','alamat','telepon'
        ));

        // Fire create event (PelangganCreated)
        event(new PelangganCreated($request));

        return redirect()->route('admin.pelanggans.index')
            ->withFlashSuccess(__('backend_pelanggans.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManagePelangganRequest  $request
     * @param Pelanggan               $pelanggan
     *
     * @return mixed
     */
    public function show(ManagePelangganRequest $request, Pelanggan $pelanggan)
    {
        return view('backend.pelanggan.show')->withPelanggan($pelanggan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManagePelangganRequest $request
     * @param Pelanggan              $pelanggan
     *
     * @return mixed
     */
    public function edit(ManagePelangganRequest $request, Pelanggan $pelanggan)
    {
        return view('backend.pelanggan.edit')->withPelanggan($pelanggan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePelangganRequest  $request
     * @param Pelanggan               $pelanggan
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdatePelangganRequest $request, Pelanggan $pelanggan)
    {
        $this->pelangganRepository->update($pelanggan, $request->only(
            'nama','alamat','telepon'
        ));

        // Fire update event (PelangganUpdated)
        event(new PelangganUpdated($request));

        return redirect()->route('admin.pelanggans.index')
            ->withFlashSuccess(__('backend_pelanggans.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManagePelangganRequest $request
     * @param Pelanggan              $pelanggan
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManagePelangganRequest $request, Pelanggan $pelanggan)
    {
        $this->pelangganRepository->deleteById($pelanggan->id);

        // Fire delete event (PelangganDeleted)
        event(new PelangganDeleted($request));

        return redirect()->route('admin.pelanggans.deleted')
            ->withFlashSuccess(__('backend_pelanggans.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManagePelangganRequest $request
     * @param Pelanggan              $deletedPelanggan
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManagePelangganRequest $request, Pelanggan $deletedPelanggan)
    {
        $this->pelangganRepository->forceDelete($deletedPelanggan);

        return redirect()->route('admin.pelanggans.deleted')
            ->withFlashSuccess(__('backend_pelanggans.alerts.deleted_permanently'));
    }

    /**
     * @param ManagePelangganRequest $request
     * @param Pelanggan              $deletedPelanggan
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManagePelangganRequest $request, Pelanggan $deletedPelanggan)
    {
        $this->pelangganRepository->restore($deletedPelanggan);

        return redirect()->route('admin.pelanggans.index')
            ->withFlashSuccess(__('backend_pelanggans.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManagePelangganRequest $request
     *
     * @return mixed
     */
    public function deleted(ManagePelangganRequest $request)
    {
        return view('backend.pelanggan.deleted')
            ->withpelanggans($this->pelangganRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
