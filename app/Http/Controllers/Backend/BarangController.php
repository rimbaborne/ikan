<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Barang;
use App\Repositories\Backend\BarangRepository;
use App\Http\Requests\Backend\Barang\ManageBarangRequest;
use App\Http\Requests\Backend\Barang\StoreBarangRequest;
use App\Http\Requests\Backend\Barang\UpdateBarangRequest;

use App\Events\Backend\Barang\BarangCreated;
use App\Events\Backend\Barang\BarangUpdated;
use App\Events\Backend\Barang\BarangDeleted;

class BarangController extends Controller
{
    /**
     * @var BarangRepository
     */
    protected $barangRepository;

    /**
     * BarangController constructor.
     *
     * @param BarangRepository $barangRepository
     */
    public function __construct(BarangRepository $barangRepository)
    {
        $this->barangRepository = $barangRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageBarangRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageBarangRequest $request)
    {
        return view('backend.barang.index')
            ->withbarangs($this->barangRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageBarangRequest    $request
     *
     * @return mixed
     */
    public function create(ManageBarangRequest $request)
    {
        return view('backend.barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBarangRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreBarangRequest $request)
    {
        $this->barangRepository->create($request->only(
            'nama','id_jenis','harga'
        ));

        // Fire create event (BarangCreated)
        event(new BarangCreated($request));

        return redirect()->route('admin.barangs.index')
            ->withFlashSuccess(__('backend_barangs.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageBarangRequest  $request
     * @param Barang               $barang
     *
     * @return mixed
     */
    public function show(ManageBarangRequest $request, Barang $barang)
    {
        return view('backend.barang.show')->withBarang($barang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageBarangRequest $request
     * @param Barang              $barang
     *
     * @return mixed
     */
    public function edit(ManageBarangRequest $request, Barang $barang)
    {
        return view('backend.barang.edit')->withBarang($barang);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBarangRequest  $request
     * @param Barang               $barang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateBarangRequest $request, Barang $barang)
    {
        $this->barangRepository->update($barang, $request->only(
            'nama','id_jenis','harga'
        ));

        // Fire update event (BarangUpdated)
        event(new BarangUpdated($request));

        return redirect()->route('admin.barangs.index')
            ->withFlashSuccess(__('backend_barangs.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageBarangRequest $request
     * @param Barang              $barang
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageBarangRequest $request, Barang $barang)
    {
        $this->barangRepository->deleteById($barang->id);

        // Fire delete event (BarangDeleted)
        event(new BarangDeleted($request));

        return redirect()->route('admin.barangs.deleted')
            ->withFlashSuccess(__('backend_barangs.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageBarangRequest $request
     * @param Barang              $deletedBarang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageBarangRequest $request, Barang $deletedBarang)
    {
        $this->barangRepository->forceDelete($deletedBarang);

        return redirect()->route('admin.barangs.deleted')
            ->withFlashSuccess(__('backend_barangs.alerts.deleted_permanently'));
    }

    /**
     * @param ManageBarangRequest $request
     * @param Barang              $deletedBarang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageBarangRequest $request, Barang $deletedBarang)
    {
        $this->barangRepository->restore($deletedBarang);

        return redirect()->route('admin.barangs.index')
            ->withFlashSuccess(__('backend_barangs.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageBarangRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageBarangRequest $request)
    {
        return view('backend.barang.deleted')
            ->withbarangs($this->barangRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
