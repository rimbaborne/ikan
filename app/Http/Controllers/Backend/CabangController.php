<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Cabang;
use App\Repositories\Backend\CabangRepository;
use App\Http\Requests\Backend\Cabang\ManageCabangRequest;
use App\Http\Requests\Backend\Cabang\StoreCabangRequest;
use App\Http\Requests\Backend\Cabang\UpdateCabangRequest;

use App\Events\Backend\Cabang\CabangCreated;
use App\Events\Backend\Cabang\CabangUpdated;
use App\Events\Backend\Cabang\CabangDeleted;

class CabangController extends Controller
{
    /**
     * @var CabangRepository
     */
    protected $cabangRepository;

    /**
     * CabangController constructor.
     *
     * @param CabangRepository $cabangRepository
     */
    public function __construct(CabangRepository $cabangRepository)
    {
        $this->cabangRepository = $cabangRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageCabangRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageCabangRequest $request)
    {
        return view('backend.cabang.index')
            ->withcabangs($this->cabangRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageCabangRequest    $request
     *
     * @return mixed
     */
    public function create(ManageCabangRequest $request)
    {
        return view('backend.cabang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCabangRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreCabangRequest $request)
    {
        $this->cabangRepository->create($request->only(
            'nama','alamat','telepon','pimpinan'
        ));

        // Fire create event (CabangCreated)
        event(new CabangCreated($request));

        return redirect()->route('admin.cabangs.index')
            ->withFlashSuccess(__('backend_cabangs.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageCabangRequest  $request
     * @param Cabang               $cabang
     *
     * @return mixed
     */
    public function show(ManageCabangRequest $request, Cabang $cabang)
    {
        return view('backend.cabang.show')->withCabang($cabang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageCabangRequest $request
     * @param Cabang              $cabang
     *
     * @return mixed
     */
    public function edit(ManageCabangRequest $request, Cabang $cabang)
    {
        return view('backend.cabang.edit')->withCabang($cabang);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateCabangRequest  $request
     * @param Cabang               $cabang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateCabangRequest $request, Cabang $cabang)
    {
        $this->cabangRepository->update($cabang, $request->only(
            'nama','alamat','telepon','pimpinan'
        ));

        // Fire update event (CabangUpdated)
        event(new CabangUpdated($request));

        return redirect()->route('admin.cabangs.index')
            ->withFlashSuccess(__('backend_cabangs.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageCabangRequest $request
     * @param Cabang              $cabang
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageCabangRequest $request, Cabang $cabang)
    {
        $this->cabangRepository->deleteById($cabang->id);

        // Fire delete event (CabangDeleted)
        event(new CabangDeleted($request));

        return redirect()->route('admin.cabangs.deleted')
            ->withFlashSuccess(__('backend_cabangs.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageCabangRequest $request
     * @param Cabang              $deletedCabang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageCabangRequest $request, Cabang $deletedCabang)
    {
        $this->cabangRepository->forceDelete($deletedCabang);

        return redirect()->route('admin.cabangs.deleted')
            ->withFlashSuccess(__('backend_cabangs.alerts.deleted_permanently'));
    }

    /**
     * @param ManageCabangRequest $request
     * @param Cabang              $deletedCabang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageCabangRequest $request, Cabang $deletedCabang)
    {
        $this->cabangRepository->restore($deletedCabang);

        return redirect()->route('admin.cabangs.index')
            ->withFlashSuccess(__('backend_cabangs.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageCabangRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageCabangRequest $request)
    {
        return view('backend.cabang.deleted')
            ->withcabangs($this->cabangRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
