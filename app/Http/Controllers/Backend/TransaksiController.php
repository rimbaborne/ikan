<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Transaksi;
use App\Repositories\Backend\TransaksiRepository;
use App\Http\Requests\Backend\Transaksi\ManageTransaksiRequest;
use App\Http\Requests\Backend\Transaksi\StoreTransaksiRequest;
use App\Http\Requests\Backend\Transaksi\UpdateTransaksiRequest;

use App\Events\Backend\Transaksi\TransaksiCreated;
use App\Events\Backend\Transaksi\TransaksiUpdated;
use App\Events\Backend\Transaksi\TransaksiDeleted;

class TransaksiController extends Controller
{
    /**
     * @var TransaksiRepository
     */
    protected $transaksiRepository;

    /**
     * TransaksiController constructor.
     *
     * @param TransaksiRepository $transaksiRepository
     */
    public function __construct(TransaksiRepository $transaksiRepository)
    {
        $this->transaksiRepository = $transaksiRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageTransaksiRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageTransaksiRequest $request)
    {
        return view('backend.transaksi.index')
            ->withtransaksis($this->transaksiRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageTransaksiRequest    $request
     *
     * @return mixed
     */
    public function create(ManageTransaksiRequest $request)
    {
        return view('backend.transaksi.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreTransaksiRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreTransaksiRequest $request)
    {
        $this->transaksiRepository->create($request->only(
            'title'
        ));

        // Fire create event (TransaksiCreated)
        event(new TransaksiCreated($request));

        return redirect()->route('admin.transaksis.index')
            ->withFlashSuccess(__('backend_transaksis.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageTransaksiRequest  $request
     * @param Transaksi               $transaksi
     *
     * @return mixed
     */
    public function show(ManageTransaksiRequest $request, Transaksi $transaksi)
    {
        return view('backend.transaksi.show')->withTransaksi($transaksi);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageTransaksiRequest $request
     * @param Transaksi              $transaksi
     *
     * @return mixed
     */
    public function edit(ManageTransaksiRequest $request, Transaksi $transaksi)
    {
        return view('backend.transaksi.edit')->withTransaksi($transaksi);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateTransaksiRequest  $request
     * @param Transaksi               $transaksi
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateTransaksiRequest $request, Transaksi $transaksi)
    {
        $this->transaksiRepository->update($transaksi, $request->only(
            'title'
        ));

        // Fire update event (TransaksiUpdated)
        event(new TransaksiUpdated($request));

        return redirect()->route('admin.transaksis.index')
            ->withFlashSuccess(__('backend_transaksis.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageTransaksiRequest $request
     * @param Transaksi              $transaksi
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageTransaksiRequest $request, Transaksi $transaksi)
    {
        $this->transaksiRepository->deleteById($transaksi->id);

        // Fire delete event (TransaksiDeleted)
        event(new TransaksiDeleted($request));

        return redirect()->route('admin.transaksis.deleted')
            ->withFlashSuccess(__('backend_transaksis.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageTransaksiRequest $request
     * @param Transaksi              $deletedTransaksi
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageTransaksiRequest $request, Transaksi $deletedTransaksi)
    {
        $this->transaksiRepository->forceDelete($deletedTransaksi);

        return redirect()->route('admin.transaksis.deleted')
            ->withFlashSuccess(__('backend_transaksis.alerts.deleted_permanently'));
    }

    /**
     * @param ManageTransaksiRequest $request
     * @param Transaksi              $deletedTransaksi
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageTransaksiRequest $request, Transaksi $deletedTransaksi)
    {
        $this->transaksiRepository->restore($deletedTransaksi);

        return redirect()->route('admin.transaksis.index')
            ->withFlashSuccess(__('backend_transaksis.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageTransaksiRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageTransaksiRequest $request)
    {
        return view('backend.transaksi.deleted')
            ->withtransaksis($this->transaksiRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
