<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\JenisBarang;
use App\Repositories\Backend\JenisBarangRepository;
use App\Http\Requests\Backend\JenisBarang\ManageJenisBarangRequest;
use App\Http\Requests\Backend\JenisBarang\StoreJenisBarangRequest;
use App\Http\Requests\Backend\JenisBarang\UpdateJenisBarangRequest;

use App\Events\Backend\JenisBarang\JenisBarangCreated;
use App\Events\Backend\JenisBarang\JenisBarangUpdated;
use App\Events\Backend\JenisBarang\JenisBarangDeleted;

class JenisBarangController extends Controller
{
    /**
     * @var JenisBarangRepository
     */
    protected $jenis_barangRepository;

    /**
     * JenisBarangController constructor.
     *
     * @param JenisBarangRepository $jenis_barangRepository
     */
    public function __construct(JenisBarangRepository $jenis_barangRepository)
    {
        $this->jenis_barangRepository = $jenis_barangRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageJenisBarangRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageJenisBarangRequest $request)
    {
        return view('backend.jenis_barang.index')
            ->withjenisBarangs($this->jenis_barangRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageJenisBarangRequest    $request
     *
     * @return mixed
     */
    public function create(ManageJenisBarangRequest $request)
    {
        return view('backend.jenis_barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreJenisBarangRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreJenisBarangRequest $request)
    {
        $this->jenis_barangRepository->create($request->only(
            'nama'
        ));

        // Fire create event (JenisBarangCreated)
        event(new JenisBarangCreated($request));

        return redirect()->route('admin.jenis_barangs.index')
            ->withFlashSuccess(__('backend_jenis_barangs.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageJenisBarangRequest  $request
     * @param JenisBarang               $jenisBarang
     *
     * @return mixed
     */
    public function show(ManageJenisBarangRequest $request, JenisBarang $jenisBarang)
    {
        return view('backend.jenis_barang.show')->withJenisBarang($jenisBarang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageJenisBarangRequest $request
     * @param JenisBarang              $jenisBarang
     *
     * @return mixed
     */
    public function edit(ManageJenisBarangRequest $request, JenisBarang $jenisBarang)
    {
        return view('backend.jenis_barang.edit')->withJenisBarang($jenisBarang);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateJenisBarangRequest  $request
     * @param JenisBarang               $jenisBarang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateJenisBarangRequest $request, JenisBarang $jenisBarang)
    {
        $this->jenis_barangRepository->update($jenisBarang, $request->only(
            'nama'
        ));

        // Fire update event (JenisBarangUpdated)
        event(new JenisBarangUpdated($request));

        return redirect()->route('admin.jenis_barangs.index')
            ->withFlashSuccess(__('backend_jenis_barangs.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageJenisBarangRequest $request
     * @param JenisBarang              $jenisBarang
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageJenisBarangRequest $request, JenisBarang $jenisBarang)
    {
        $this->jenis_barangRepository->deleteById($jenisBarang->id);

        // Fire delete event (JenisBarangDeleted)
        event(new JenisBarangDeleted($request));

        return redirect()->route('admin.jenis_barangs.deleted')
            ->withFlashSuccess(__('backend_jenis_barangs.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageJenisBarangRequest $request
     * @param JenisBarang              $deletedJenisBarang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageJenisBarangRequest $request, JenisBarang $deletedJenisBarang)
    {
        $this->jenis_barangRepository->forceDelete($deletedJenisBarang);

        return redirect()->route('admin.jenis_barangs.deleted')
            ->withFlashSuccess(__('backend_jenis_barangs.alerts.deleted_permanently'));
    }

    /**
     * @param ManageJenisBarangRequest $request
     * @param JenisBarang              $deletedJenisBarang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageJenisBarangRequest $request, JenisBarang $deletedJenisBarang)
    {
        $this->jenis_barangRepository->restore($deletedJenisBarang);

        return redirect()->route('admin.jenis_barangs.index')
            ->withFlashSuccess(__('backend_jenis_barangs.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageJenisBarangRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageJenisBarangRequest $request)
    {
        return view('backend.jenis_barang.deleted')
            ->withjenisBarangs($this->jenis_barangRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
