<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Karyawan;
use App\Repositories\Backend\KaryawanRepository;
use App\Http\Requests\Backend\Karyawan\ManageKaryawanRequest;
use App\Http\Requests\Backend\Karyawan\StoreKaryawanRequest;
use App\Http\Requests\Backend\Karyawan\UpdateKaryawanRequest;

use App\Events\Backend\Karyawan\KaryawanCreated;
use App\Events\Backend\Karyawan\KaryawanUpdated;
use App\Events\Backend\Karyawan\KaryawanDeleted;

class KaryawanController extends Controller
{
    /**
     * @var KaryawanRepository
     */
    protected $karyawanRepository;

    /**
     * KaryawanController constructor.
     *
     * @param KaryawanRepository $karyawanRepository
     */
    public function __construct(KaryawanRepository $karyawanRepository)
    {
        $this->karyawanRepository = $karyawanRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageKaryawanRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageKaryawanRequest $request)
    {
        return view('backend.karyawan.index')
            ->withkaryawans($this->karyawanRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageKaryawanRequest    $request
     *
     * @return mixed
     */
    public function create(ManageKaryawanRequest $request)
    {
        return view('backend.karyawan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreKaryawanRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreKaryawanRequest $request)
    {
        $this->karyawanRepository->create($request->only(
        'nama',
        'jenis_kelamin',
        'jabatan',
        'alamat',
        'telepon',
        'id_cabang',
        'id_user',
        'id_pengguna',
        ));

        // Fire create event (KaryawanCreated)
        event(new KaryawanCreated($request));

        return redirect()->route('admin.karyawans.index')
            ->withFlashSuccess(__('backend_karyawans.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageKaryawanRequest  $request
     * @param Karyawan               $karyawan
     *
     * @return mixed
     */
    public function show(ManageKaryawanRequest $request, Karyawan $karyawan)
    {
        return view('backend.karyawan.show')->withKaryawan($karyawan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageKaryawanRequest $request
     * @param Karyawan              $karyawan
     *
     * @return mixed
     */
    public function edit(ManageKaryawanRequest $request, Karyawan $karyawan)
    {
        return view('backend.karyawan.edit')->withKaryawan($karyawan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateKaryawanRequest  $request
     * @param Karyawan               $karyawan
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateKaryawanRequest $request, Karyawan $karyawan)
    {
        $this->karyawanRepository->update($karyawan, $request->only(
        'nama',
        'jenis_kelamin',
        'jabatan',
        'alamat',
        'telepon',
        'id_cabang',
        'id_user',
        'id_pengguna',
        ));

        // Fire update event (KaryawanUpdated)
        event(new KaryawanUpdated($request));

        return redirect()->route('admin.karyawans.index')
            ->withFlashSuccess(__('backend_karyawans.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageKaryawanRequest $request
     * @param Karyawan              $karyawan
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageKaryawanRequest $request, Karyawan $karyawan)
    {
        $this->karyawanRepository->deleteById($karyawan->id);

        // Fire delete event (KaryawanDeleted)
        event(new KaryawanDeleted($request));

        return redirect()->route('admin.karyawans.deleted')
            ->withFlashSuccess(__('backend_karyawans.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageKaryawanRequest $request
     * @param Karyawan              $deletedKaryawan
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageKaryawanRequest $request, Karyawan $deletedKaryawan)
    {
        $this->karyawanRepository->forceDelete($deletedKaryawan);

        return redirect()->route('admin.karyawans.deleted')
            ->withFlashSuccess(__('backend_karyawans.alerts.deleted_permanently'));
    }

    /**
     * @param ManageKaryawanRequest $request
     * @param Karyawan              $deletedKaryawan
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageKaryawanRequest $request, Karyawan $deletedKaryawan)
    {
        $this->karyawanRepository->restore($deletedKaryawan);

        return redirect()->route('admin.karyawans.index')
            ->withFlashSuccess(__('backend_karyawans.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageKaryawanRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageKaryawanRequest $request)
    {
        return view('backend.karyawan.deleted')
            ->withkaryawans($this->karyawanRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
