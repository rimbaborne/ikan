<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Piutang;
use App\Repositories\Backend\PiutangRepository;
use App\Http\Requests\Backend\Piutang\ManagePiutangRequest;
use App\Http\Requests\Backend\Piutang\StorePiutangRequest;
use App\Http\Requests\Backend\Piutang\UpdatePiutangRequest;

use App\Events\Backend\Piutang\PiutangCreated;
use App\Events\Backend\Piutang\PiutangUpdated;
use App\Events\Backend\Piutang\PiutangDeleted;

class PiutangController extends Controller
{
    /**
     * @var PiutangRepository
     */
    protected $piutangRepository;

    /**
     * PiutangController constructor.
     *
     * @param PiutangRepository $piutangRepository
     */
    public function __construct(PiutangRepository $piutangRepository)
    {
        $this->piutangRepository = $piutangRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManagePiutangRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManagePiutangRequest $request)
    {
        return view('backend.piutang.index')
            ->withpiutangs($this->piutangRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManagePiutangRequest    $request
     *
     * @return mixed
     */
    public function create(ManagePiutangRequest $request)
    {
        return view('backend.piutang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePiutangRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StorePiutangRequest $request)
    {
        $this->piutangRepository->create($request->only(
            'title'
        ));

        // Fire create event (PiutangCreated)
        event(new PiutangCreated($request));

        return redirect()->route('admin.piutangs.index')
            ->withFlashSuccess(__('backend_piutangs.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManagePiutangRequest  $request
     * @param Piutang               $piutang
     *
     * @return mixed
     */
    public function show(ManagePiutangRequest $request, Piutang $piutang)
    {
        return view('backend.piutang.show')->withPiutang($piutang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManagePiutangRequest $request
     * @param Piutang              $piutang
     *
     * @return mixed
     */
    public function edit(ManagePiutangRequest $request, Piutang $piutang)
    {
        return view('backend.piutang.edit')->withPiutang($piutang);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePiutangRequest  $request
     * @param Piutang               $piutang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdatePiutangRequest $request, Piutang $piutang)
    {
        $this->piutangRepository->update($piutang, $request->only(
            'title'
        ));

        // Fire update event (PiutangUpdated)
        event(new PiutangUpdated($request));

        return redirect()->route('admin.piutangs.index')
            ->withFlashSuccess(__('backend_piutangs.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManagePiutangRequest $request
     * @param Piutang              $piutang
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManagePiutangRequest $request, Piutang $piutang)
    {
        $this->piutangRepository->deleteById($piutang->id);

        // Fire delete event (PiutangDeleted)
        event(new PiutangDeleted($request));

        return redirect()->route('admin.piutangs.deleted')
            ->withFlashSuccess(__('backend_piutangs.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManagePiutangRequest $request
     * @param Piutang              $deletedPiutang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManagePiutangRequest $request, Piutang $deletedPiutang)
    {
        $this->piutangRepository->forceDelete($deletedPiutang);

        return redirect()->route('admin.piutangs.deleted')
            ->withFlashSuccess(__('backend_piutangs.alerts.deleted_permanently'));
    }

    /**
     * @param ManagePiutangRequest $request
     * @param Piutang              $deletedPiutang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManagePiutangRequest $request, Piutang $deletedPiutang)
    {
        $this->piutangRepository->restore($deletedPiutang);

        return redirect()->route('admin.piutangs.index')
            ->withFlashSuccess(__('backend_piutangs.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManagePiutangRequest $request
     *
     * @return mixed
     */
    public function deleted(ManagePiutangRequest $request)
    {
        return view('backend.piutang.deleted')
            ->withpiutangs($this->piutangRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
