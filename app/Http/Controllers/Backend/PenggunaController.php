<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Pengguna;
use App\Repositories\Backend\PenggunaRepository;
use App\Http\Requests\Backend\Pengguna\ManagePenggunaRequest;
use App\Http\Requests\Backend\Pengguna\StorePenggunaRequest;
use App\Http\Requests\Backend\Pengguna\UpdatePenggunaRequest;

use App\Events\Backend\Pengguna\PenggunaCreated;
use App\Events\Backend\Pengguna\PenggunaUpdated;
use App\Events\Backend\Pengguna\PenggunaDeleted;

class PenggunaController extends Controller
{
    /**
     * @var PenggunaRepository
     */
    protected $penggunaRepository;

    /**
     * PenggunaController constructor.
     *
     * @param PenggunaRepository $penggunaRepository
     */
    public function __construct(PenggunaRepository $penggunaRepository)
    {
        $this->penggunaRepository = $penggunaRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManagePenggunaRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManagePenggunaRequest $request)
    {
        return view('backend.pengguna.index')
            ->withpenggunas($this->penggunaRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManagePenggunaRequest    $request
     *
     * @return mixed
     */
    public function create(ManagePenggunaRequest $request)
    {
        return view('backend.pengguna.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePenggunaRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StorePenggunaRequest $request)
    {
        $this->penggunaRepository->create($request->only(
            'title'
        ));

        // Fire create event (PenggunaCreated)
        event(new PenggunaCreated($request));

        return redirect()->route('admin.penggunas.index')
            ->withFlashSuccess(__('backend_penggunas.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManagePenggunaRequest  $request
     * @param Pengguna               $pengguna
     *
     * @return mixed
     */
    public function show(ManagePenggunaRequest $request, Pengguna $pengguna)
    {
        return view('backend.pengguna.show')->withPengguna($pengguna);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManagePenggunaRequest $request
     * @param Pengguna              $pengguna
     *
     * @return mixed
     */
    public function edit(ManagePenggunaRequest $request, Pengguna $pengguna)
    {
        return view('backend.pengguna.edit')->withPengguna($pengguna);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePenggunaRequest  $request
     * @param Pengguna               $pengguna
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdatePenggunaRequest $request, Pengguna $pengguna)
    {
        $this->penggunaRepository->update($pengguna, $request->only(
            'title'
        ));

        // Fire update event (PenggunaUpdated)
        event(new PenggunaUpdated($request));

        return redirect()->route('admin.penggunas.index')
            ->withFlashSuccess(__('backend_penggunas.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManagePenggunaRequest $request
     * @param Pengguna              $pengguna
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManagePenggunaRequest $request, Pengguna $pengguna)
    {
        $this->penggunaRepository->deleteById($pengguna->id);

        // Fire delete event (PenggunaDeleted)
        event(new PenggunaDeleted($request));

        return redirect()->route('admin.penggunas.deleted')
            ->withFlashSuccess(__('backend_penggunas.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManagePenggunaRequest $request
     * @param Pengguna              $deletedPengguna
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManagePenggunaRequest $request, Pengguna $deletedPengguna)
    {
        $this->penggunaRepository->forceDelete($deletedPengguna);

        return redirect()->route('admin.penggunas.deleted')
            ->withFlashSuccess(__('backend_penggunas.alerts.deleted_permanently'));
    }

    /**
     * @param ManagePenggunaRequest $request
     * @param Pengguna              $deletedPengguna
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManagePenggunaRequest $request, Pengguna $deletedPengguna)
    {
        $this->penggunaRepository->restore($deletedPengguna);

        return redirect()->route('admin.penggunas.index')
            ->withFlashSuccess(__('backend_penggunas.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManagePenggunaRequest $request
     *
     * @return mixed
     */
    public function deleted(ManagePenggunaRequest $request)
    {
        return view('backend.pengguna.deleted')
            ->withpenggunas($this->penggunaRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
