<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\StokBarang;
use App\Repositories\Backend\StokBarangRepository;
use App\Http\Requests\Backend\StokBarang\ManageStokBarangRequest;
use App\Http\Requests\Backend\StokBarang\StoreStokBarangRequest;
use App\Http\Requests\Backend\StokBarang\UpdateStokBarangRequest;

use App\Events\Backend\StokBarang\StokBarangCreated;
use App\Events\Backend\StokBarang\StokBarangUpdated;
use App\Events\Backend\StokBarang\StokBarangDeleted;

class StokBarangController extends Controller
{
    /**
     * @var StokBarangRepository
     */
    protected $stok_barangRepository;

    /**
     * StokBarangController constructor.
     *
     * @param StokBarangRepository $stok_barangRepository
     */
    public function __construct(StokBarangRepository $stok_barangRepository)
    {
        $this->stok_barangRepository = $stok_barangRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageStokBarangRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageStokBarangRequest $request)
    {
        return view('backend.stok_barang.index')
            ->withstokBarangs($this->stok_barangRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageStokBarangRequest    $request
     *
     * @return mixed
     */
    public function create(ManageStokBarangRequest $request)
    {
        return view('backend.stok_barang.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreStokBarangRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreStokBarangRequest $request)
    {
        $this->stok_barangRepository->create($request->only(
            'title'
        ));

        // Fire create event (StokBarangCreated)
        event(new StokBarangCreated($request));

        return redirect()->route('admin.stok_barangs.index')
            ->withFlashSuccess(__('backend_stok_barangs.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageStokBarangRequest  $request
     * @param StokBarang               $stokBarang
     *
     * @return mixed
     */
    public function show(ManageStokBarangRequest $request, StokBarang $stokBarang)
    {
        return view('backend.stok_barang.show')->withStokBarang($stokBarang);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageStokBarangRequest $request
     * @param StokBarang              $stokBarang
     *
     * @return mixed
     */
    public function edit(ManageStokBarangRequest $request, StokBarang $stokBarang)
    {
        return view('backend.stok_barang.edit')->withStokBarang($stokBarang);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateStokBarangRequest  $request
     * @param StokBarang               $stokBarang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateStokBarangRequest $request, StokBarang $stokBarang)
    {
        $this->stok_barangRepository->update($stokBarang, $request->only(
            'title'
        ));

        // Fire update event (StokBarangUpdated)
        event(new StokBarangUpdated($request));

        return redirect()->route('admin.stok_barangs.index')
            ->withFlashSuccess(__('backend_stok_barangs.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageStokBarangRequest $request
     * @param StokBarang              $stokBarang
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageStokBarangRequest $request, StokBarang $stokBarang)
    {
        $this->stok_barangRepository->deleteById($stokBarang->id);

        // Fire delete event (StokBarangDeleted)
        event(new StokBarangDeleted($request));

        return redirect()->route('admin.stok_barangs.deleted')
            ->withFlashSuccess(__('backend_stok_barangs.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageStokBarangRequest $request
     * @param StokBarang              $deletedStokBarang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageStokBarangRequest $request, StokBarang $deletedStokBarang)
    {
        $this->stok_barangRepository->forceDelete($deletedStokBarang);

        return redirect()->route('admin.stok_barangs.deleted')
            ->withFlashSuccess(__('backend_stok_barangs.alerts.deleted_permanently'));
    }

    /**
     * @param ManageStokBarangRequest $request
     * @param StokBarang              $deletedStokBarang
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageStokBarangRequest $request, StokBarang $deletedStokBarang)
    {
        $this->stok_barangRepository->restore($deletedStokBarang);

        return redirect()->route('admin.stok_barangs.index')
            ->withFlashSuccess(__('backend_stok_barangs.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageStokBarangRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageStokBarangRequest $request)
    {
        return view('backend.stok_barang.deleted')
            ->withstokBarangs($this->stok_barangRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
