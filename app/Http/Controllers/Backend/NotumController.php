<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Exceptions\GeneralException;

use App\Models\Notum;
use App\Repositories\Backend\NotumRepository;
use App\Http\Requests\Backend\Notum\ManageNotumRequest;
use App\Http\Requests\Backend\Notum\StoreNotumRequest;
use App\Http\Requests\Backend\Notum\UpdateNotumRequest;

use App\Events\Backend\Notum\NotumCreated;
use App\Events\Backend\Notum\NotumUpdated;
use App\Events\Backend\Notum\NotumDeleted;

class NotumController extends Controller
{
    /**
     * @var NotumRepository
     */
    protected $notumRepository;

    /**
     * NotumController constructor.
     *
     * @param NotumRepository $notumRepository
     */
    public function __construct(NotumRepository $notumRepository)
    {
        $this->notumRepository = $notumRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param ManageNotumRequest $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageNotumRequest $request)
    {
        return view('backend.notum.index')
            ->withnota($this->notumRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param ManageNotumRequest    $request
     *
     * @return mixed
     */
    public function create(ManageNotumRequest $request)
    {
        return view('backend.notum.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreNotumRequest $request
     *
     * @return mixed
     * @throws \Throwable
     */
    public function store(StoreNotumRequest $request)
    {
        $this->notumRepository->create($request->only(
            'title'
        ));

        // Fire create event (NotumCreated)
        event(new NotumCreated($request));

        return redirect()->route('admin.nota.index')
            ->withFlashSuccess(__('backend_nota.alerts.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param ManageNotumRequest  $request
     * @param Notum               $notum
     *
     * @return mixed
     */
    public function show(ManageNotumRequest $request, Notum $notum)
    {
        return view('backend.notum.show')->withNotum($notum);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param ManageNotumRequest $request
     * @param Notum              $notum
     *
     * @return mixed
     */
    public function edit(ManageNotumRequest $request, Notum $notum)
    {
        return view('backend.notum.edit')->withNotum($notum);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateNotumRequest  $request
     * @param Notum               $notum
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function update(UpdateNotumRequest $request, Notum $notum)
    {
        $this->notumRepository->update($notum, $request->only(
            'title'
        ));

        // Fire update event (NotumUpdated)
        event(new NotumUpdated($request));

        return redirect()->route('admin.nota.index')
            ->withFlashSuccess(__('backend_nota.alerts.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ManageNotumRequest $request
     * @param Notum              $notum
     *
     * @return mixed
     * @throws \Exception
     */
    public function destroy(ManageNotumRequest $request, Notum $notum)
    {
        $this->notumRepository->deleteById($notum->id);

        // Fire delete event (NotumDeleted)
        event(new NotumDeleted($request));

        return redirect()->route('admin.nota.deleted')
            ->withFlashSuccess(__('backend_nota.alerts.deleted'));
    }

    /**
     * Permanently remove the specified resource from storage.
     *
     * @param ManageNotumRequest $request
     * @param Notum              $deletedNotum
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     * @throws \Throwable
     */
    public function delete(ManageNotumRequest $request, Notum $deletedNotum)
    {
        $this->notumRepository->forceDelete($deletedNotum);

        return redirect()->route('admin.nota.deleted')
            ->withFlashSuccess(__('backend_nota.alerts.deleted_permanently'));
    }

    /**
     * @param ManageNotumRequest $request
     * @param Notum              $deletedNotum
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManageNotumRequest $request, Notum $deletedNotum)
    {
        $this->notumRepository->restore($deletedNotum);

        return redirect()->route('admin.nota.index')
            ->withFlashSuccess(__('backend_nota.alerts.restored'));
    }

    /**
     * Display a listing of deleted items of the resource.
     *
     * @param ManageNotumRequest $request
     *
     * @return mixed
     */
    public function deleted(ManageNotumRequest $request)
    {
        return view('backend.notum.deleted')
            ->withnota($this->notumRepository->getDeletedPaginated(25, 'id', 'asc'));
    }
}
