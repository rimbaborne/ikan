<?php

namespace App\Http\Requests\Backend\Barang;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateBarangRequest.
 */
class UpdateBarangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'     => ['required', 'max:191'],
            'id_jenis' => ['required', 'max:3'],
            'harga'    => ['required', 'max:191'],
        ];
    }

    /**
     * Custom validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama.required'    => 'The :attribute field is required.',
            'nama.max'         => 'The :attribute field must have less than :max characters',
            'harga.required'   => 'The :attribute field is required.',
            'harga.max'        => 'The :attribute field must have less than :max characters',
        ];
    }
}
