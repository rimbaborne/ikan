<?php

namespace App\Http\Requests\Backend\Cabang;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateCabangRequest.
 */
class UpdateCabangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'     => ['required', 'max:191'],
            'telepon'  => ['required', 'max:14'],
            'pimpinan'  => ['required', 'max:100'],
        ];
    }

    /**
     * Custom validation messages.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nama.required'    => 'The :attribute field is required.',
            'nama.max'         => 'The :attribute field must have less than :max characters',
            'telepon.max'      => 'The :attribute field must have less than :max characters',
            'pimpinan.required'    => 'The :attribute field is required.',
            'pimpinan.max'      => 'The :attribute field must have less than :max characters',

        ];
    }
}
