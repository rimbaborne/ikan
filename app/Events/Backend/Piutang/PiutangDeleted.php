<?php

namespace App\Events\Backend\Piutang;

use Illuminate\Queue\SerializesModels;

/**
 * Class PiutangDeleted.
 */
class PiutangDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $piutangs;

    /**
     * @param $piutangs
     */
    public function __construct($piutangs)
    {
        $this->piutangs = $piutangs;
    }
}
