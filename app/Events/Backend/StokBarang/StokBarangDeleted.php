<?php

namespace App\Events\Backend\StokBarang;

use Illuminate\Queue\SerializesModels;

/**
 * Class StokBarangDeleted.
 */
class StokBarangDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $stok_barangs;

    /**
     * @param $stok_barangs
     */
    public function __construct($stok_barangs)
    {
        $this->stok_barangs = $stok_barangs;
    }
}
