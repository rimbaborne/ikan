<?php

namespace App\Events\Backend\Pengguna;

use Illuminate\Queue\SerializesModels;

/**
 * Class PenggunaUpdated.
 */
class PenggunaUpdated
{
    use SerializesModels;

    /**
     * @var
     */
    public $penggunas;

    /**
     * @param $penggunas
     */
    public function __construct($penggunas)
    {
        $this->penggunas = $penggunas;
    }
}
