<?php

namespace App\Events\Backend\Notum;

use Illuminate\Queue\SerializesModels;

/**
 * Class NotumDeleted.
 */
class NotumDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $nota;

    /**
     * @param $nota
     */
    public function __construct($nota)
    {
        $this->nota = $nota;
    }
}
