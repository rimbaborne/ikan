<?php

namespace App\Events\Backend\Barang;

use Illuminate\Queue\SerializesModels;

/**
 * Class BarangDeleted.
 */
class BarangDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $barangs;

    /**
     * @param $barangs
     */
    public function __construct($barangs)
    {
        $this->barangs = $barangs;
    }
}
