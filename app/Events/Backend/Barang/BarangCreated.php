<?php

namespace App\Events\Backend\Barang;

use Illuminate\Queue\SerializesModels;

/**
 * Class BarangCreated.
 */
class BarangCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $barangs;

    /**
     * @param $barangs
     */
    public function __construct($barangs)
    {
        $this->barangs = $barangs;
    }
}
