<?php

namespace App\Events\Backend\Cabang;

use Illuminate\Queue\SerializesModels;

/**
 * Class CabangCreated.
 */
class CabangCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $cabangs;

    /**
     * @param $cabangs
     */
    public function __construct($cabangs)
    {
        $this->cabangs = $cabangs;
    }
}
