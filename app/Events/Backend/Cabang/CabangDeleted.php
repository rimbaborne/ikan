<?php

namespace App\Events\Backend\Cabang;

use Illuminate\Queue\SerializesModels;

/**
 * Class CabangDeleted.
 */
class CabangDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $cabangs;

    /**
     * @param $cabangs
     */
    public function __construct($cabangs)
    {
        $this->cabangs = $cabangs;
    }
}
