<?php

namespace App\Events\Backend\Transaksi;

use Illuminate\Queue\SerializesModels;

/**
 * Class TransaksiDeleted.
 */
class TransaksiDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $transaksis;

    /**
     * @param $transaksis
     */
    public function __construct($transaksis)
    {
        $this->transaksis = $transaksis;
    }
}
