<?php

namespace App\Events\Backend\Transaksi;

use Illuminate\Queue\SerializesModels;

/**
 * Class TransaksiCreated.
 */
class TransaksiCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $transaksis;

    /**
     * @param $transaksis
     */
    public function __construct($transaksis)
    {
        $this->transaksis = $transaksis;
    }
}
