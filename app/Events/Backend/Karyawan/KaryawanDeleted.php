<?php

namespace App\Events\Backend\Karyawan;

use Illuminate\Queue\SerializesModels;

/**
 * Class KaryawanDeleted.
 */
class KaryawanDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $karyawans;

    /**
     * @param $karyawans
     */
    public function __construct($karyawans)
    {
        $this->karyawans = $karyawans;
    }
}
