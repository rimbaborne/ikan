<?php

namespace App\Events\Backend\Karyawan;

use Illuminate\Queue\SerializesModels;

/**
 * Class KaryawanCreated.
 */
class KaryawanCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $karyawans;

    /**
     * @param $karyawans
     */
    public function __construct($karyawans)
    {
        $this->karyawans = $karyawans;
    }
}
