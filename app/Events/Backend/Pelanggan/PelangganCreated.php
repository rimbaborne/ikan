<?php

namespace App\Events\Backend\Pelanggan;

use Illuminate\Queue\SerializesModels;

/**
 * Class PelangganCreated.
 */
class PelangganCreated
{
    use SerializesModels;

    /**
     * @var
     */
    public $pelanggans;

    /**
     * @param $pelanggans
     */
    public function __construct($pelanggans)
    {
        $this->pelanggans = $pelanggans;
    }
}
