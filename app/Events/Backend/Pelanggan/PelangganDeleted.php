<?php

namespace App\Events\Backend\Pelanggan;

use Illuminate\Queue\SerializesModels;

/**
 * Class PelangganDeleted.
 */
class PelangganDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $pelanggans;

    /**
     * @param $pelanggans
     */
    public function __construct($pelanggans)
    {
        $this->pelanggans = $pelanggans;
    }
}
