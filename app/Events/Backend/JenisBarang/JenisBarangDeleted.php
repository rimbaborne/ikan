<?php

namespace App\Events\Backend\JenisBarang;

use Illuminate\Queue\SerializesModels;

/**
 * Class JenisBarangDeleted.
 */
class JenisBarangDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $jenis_barangs;

    /**
     * @param $jenis_barangs
     */
    public function __construct($jenis_barangs)
    {
        $this->jenis_barangs = $jenis_barangs;
    }
}
